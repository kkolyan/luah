# About

Wrapper for embedding LuaJIT into C# applications.

# FAQ

## System.DllNotFoundException : Unable to load DLL

platform specific Lua shared library (`lua51.dll` or `liblua51.so`) should be manually put to `luah_test/bin/Debug` (or `luah_test/bin/Release`) directory before running any tests or examples.

## Reference version of LuaJIT
https://github.com/LuaJIT/LuaJIT.git 3f9389edc6cdf3f78a6896d550c236860aed62b2
