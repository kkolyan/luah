using System;
using System.Collections.Generic;
using System.Linq;

namespace Luah.Test
{
    public class GetCalleeLineExample
    {
        public static void Main()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();

                state.lua_pushcfunction(DoDoRoDoDo);
                state.lua_setglobal("test1");

                state.eval($@"
                function test2()
                    test1()
                end
                test2()
                ", 0, 0);
            }
        }

        private static int DoDoRoDoDo(LuaState scope)
        {
            List<string> breadcrumbles = new List<string>(); 
            for (int i = 0; i < 100; i++)
            {
                LuaDebug ar;
                unsafe
                {
                    if (scope.lua_getstack(i, &ar) == 0)
                    {
                        break;
                    }
                    scope.lua_getinfo("Sl", &ar);
                }

                breadcrumbles.Add($"{ar.short_src_AsString}:{ar.currentline}");
            }
            Console.WriteLine(breadcrumbles);

            return 0;
        }
    }
}