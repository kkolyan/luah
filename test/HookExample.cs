using System;

namespace Luah.Test
{
    public class HookExample
    {
        private static string[] eventNames =
        {
            "LUA_HOOKCALL",
            "LUA_HOOKRET",
            "LUA_HOOKLINE",
            "LUA_HOOKCOUNT",
            "LUA_HOOKTAILRET",
        };
        public static void Main()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                unsafe
                {
                    state.lua_sethook(Hook, Lua.LUA_HOOKLINE | Lua.LUA_HOOKCALL | Lua.LUA_HOOKCOUNT | Lua.LUA_HOOKRET | Lua.LUA_HOOKTAILRET, 1);
                }
                state.luaL_openlibs();
                state.luaL_loadstring_safe(@"
                    jit.off()
                    function test1(a, b, c, d)
                        local x = a + b
                        x = x * c
                        return x + d
                    end
                    
                    return test1(4, 3, 2, 1)
                ");
                state.lua_pcall_traceback(0, 1);
                Console.WriteLine(state.lua_tonumber(-1));
            }
        }

        private static unsafe void Hook(LuaState state, LuaDebug* debug)
        {
            Console.WriteLine($"{eventNames[debug->@event]}: {debug->currentline}");
        }
    }
}