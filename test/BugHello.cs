using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.AccessControl;

namespace Luah.Test
{
    public class MyObject
    {
        
    }
    public static class BugHello
    {
        private static readonly ReferenceBoard Refs = new ReferenceBoard();
        private static int MethodCall(LuaState state)
        {
            return 0;
        }

        private static MethodInfo ToUserDataMethod<T>()
        {
            MethodInfo generic
                = typeof(BugHello)
                      .GetMethod(
                          nameof(luaKK_touserdata),
                          new[] {typeof(LuaState), typeof(int)}
                      )
                  ?? throw new Exception("WTF?!");
            return generic.MakeGenericMethod(typeof(T));
        }

        public static T luaKK_touserdata<T>(this LuaState state, int index)
        {
            VoidPtr ptr = state.lua_touserdata(index);
            if (ptr.Value == default)
            {
                throw new Exception($"instance of {typeof(T).FullName} not found at index {index}");
            }
            object o = Refs.Get(Marshal.ReadInt64(ptr.Value));
            if (o is T t)
            {
                return t;
            }

            throw new Exception($"cannot cast {o.GetType().FullName} to {typeof(T).FullName}. object: {o}");
        }

        public static void Main()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                MethodInfo methodInfo = ((LuaCFunction) MethodCall).Method;

                ParameterExpression stateParam = Expression.Parameter(typeof(LuaState));

                MethodCallExpression wrapperMethodCall;
                if (methodInfo.IsStatic)
                {
                    wrapperMethodCall = Expression.Call(
                        methodInfo,
                        stateParam
                    );
                }
                else
                {
                    wrapperMethodCall = Expression.Call(
                        Expression.Call(ToUserDataMethod<MyObject>(), stateParam, Expression.Constant(1)),
                        methodInfo,
                        stateParam
                    );
                }
                
                LuaCFunction function = Expression
                    .Lambda<LuaCFunction>(wrapperMethodCall, stateParam)
                    .Compile();
                
                string wrapped = "csharp__Code__LuaObjects__ImageObject__type__target";
                string wrapper = "csharp__Code__LuaObjects__ImageObject__type";

                state.lua_pushcfunction(function);
                state.lua_setglobal(wrapped);

                state.luaL_loadstring_safe($@"    
                    function {wrapper}(a,b,c,d,e,f,g,h)
                        return {wrapped}(a,b,c,d,e,f,g,h)
                    end
                ");
                state.lua_pcall_traceback(0, 0);
                
                state.eval($"{wrapper}()", 0, 0);
                Console.WriteLine("OK");
            }
        }
    }
}