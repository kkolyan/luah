namespace Luah.Test
{
    public class BufferExample
    {
        public static void Main()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();
                const string buf = @"
                lib = {}
                function lib.myFunc()
                    error('hello!')
                end
                ";
                state.TranslateResult(state.luaL_loadbuffer(buf, buf.Length, "my_module.lua"), "luaL_loadbuffer");
                state.lua_pcall_traceback(0, 1);
                state.lua_getglobal("lib");
                state.lua_getfield(-1, "myFunc");
                state.lua_pcall_traceback(0, 0);
            }
        }
    }
}