using System;

namespace Luah.Test
{
    public class StackTraceExample
    {
        public static void Main()
        {
            try
            {
                using (LuaState state = Lua.luaL_newstate())
                {
                    state.luaL_openlibs();

                    state.luaL_loadstring_safe(@"
                    function func001(m)
                        test_error(m)
                    end
                    
                    function func002(m)
                        func001(m)
                    end
                    
                    func002('hi')
                ");
                    state.lua_pcall_traceback(0, 0);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}