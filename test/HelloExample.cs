namespace Luah.Test
{
    public class HelloExample
    {
        public static void Main()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();
                state.luaL_loadstring_safe("print('Hello!')");
                state.lua_pcall_traceback(0, 0);
            }
        }
    }
}