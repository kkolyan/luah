using System;
using System.Collections.Generic;
using Luah.Debug;

namespace Luah.Test
{
    public class FunctionDebugExample
    {
        public static void Main()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();

                IDictionary<IntPtr, LuaCFunction> mapping = new Dictionary<IntPtr, LuaCFunction>();

                Function.CurrentFunctionResolver = ptr => mapping[ptr.Value];

                LuaCFunction helloFunction = Hello;
                state.lua_pushcfunction(helloFunction);
                IntPtr helloPtr = state.lua_topointer(-1).Value;

                mapping[helloPtr] = helloFunction;
                
                state.lua_setglobal("hello");
                state.lua_getglobal("hello");
                
                Console.WriteLine("Set breakpoint here and examine `state.Stack` for extra rendered info");
            }
        }

        private static int Hello(LuaState scope)
        {
            string arg = scope.luaL_checkstring(1).ManagedCopy();
            scope.lua_pushstring($"Hello {arg}!");
            return 1;
        }
    }
}