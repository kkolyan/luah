namespace Luah.Test
{
    public class MetaMethodsErrorExample
    {
        public static void IndexError()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();

                state.luaL_newmetatable("Pet");
                state.lua_pushcfunction(scope =>
                {
                    scope.lua_pushstring("Hello!");
                    return scope.lua_error();
                });
                state.lua_setfield(-2, "__newindex");

                state.lua_pop(1); // Pet


                state.lua_newuserdata(sizeof(long));
                state.luaL_setmetatable("Pet");
                state.lua_setglobal("pet");
                state.eval("pet.x = 2", 0, 0);
            }
        }
    }
}