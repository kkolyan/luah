using System;
using System.Runtime.InteropServices;

namespace Luah.Test
{
    public class ExceptionExample
    {
        public static void CrashIfCallErrorOnTopLevel()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();
                state.lua_pushstring("Hello!");
                // throws SEHException, at least on windows.
                state.lua_error();
                Console.WriteLine("error swallowed");
            }
        }

        private class MyException : Exception
        {
            public MyException(string message) : base(message) { }
        }
        
        public static void ExceptionPropagatedIfThrown()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();
                state.lua_pushcfunction(scope =>
                {
                    throw new MyException("hello!");
                });
                state.lua_setglobal("hello");
                try
                {
                    state.eval("hello()", 0, 0);
                    Console.WriteLine("error swallowed");
                }
                catch (MyException e)
                {
                    Console.WriteLine($"error propagated: {e}");
                }
            }
        }

        private static string var0;
        
        public static void ErrorConvertedToException()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();
                state.lua_pushcfunction(scope =>
                {
                    scope.lua_pushstring("hello!");
                    var0 = "before error";
                    int luaError = scope.lua_error();
                    // not invoked. lonjmp correctly affects CLR
                    var0 = "after error";
                    return luaError;
                });
                state.lua_setglobal("hello");
                try
                {
                    state.eval("hello()", 0, 0);
                    Console.WriteLine("error swallowed");
                }
                catch (LuaException e)
                {
                    Console.WriteLine($"error propagated: {e}");
                }
                Console.WriteLine($"var0: {var0}");
            }
        }
    }
}