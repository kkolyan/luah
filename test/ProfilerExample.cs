using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Luah.Test
{
    public class ProfilerExample
    {
        private static ThreadLocal<LuaState> _currentState = new ThreadLocal<LuaState>();

        private static int Hello(LuaState state)
        {
            // state.luaL_error("hello");
            return 0;
        }

        public static void Main()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                _currentState.Value = state;
                state.luaL_openlibs();
                // state.eval("jit.off()", 0, 0);
                state.eval("prof = require('jit.profile')", 0, 0);

                state.lua_pushcfunction(Hello);
                state.lua_setglobal("hello");

                EvalScript(state, @"
                    local x = 0
                    function funcA(a, b)
                        x = x + 1
                        return a + b
                    end
                ", "funcA.lua");

                state.lua_getglobal("prof");
                state.lua_getfield(-1, "start");
                state.lua_pushstring("f");
                state.lua_pushcfunction(Handle);
                state.lua_pcall_traceback(2, 0);

                state.lua_pop(1); //prof

                EvalScript(state, @"
                    local y = 0
                    function hello_()
                        if y > 0 then
                            hello()
                        end
                        y = y + 1
                    end
                    function funcB(j)
                        for i = 1, 10000 do
                            funcA(i, j)
                            hello_()
                        end
                    end
                    function funcC()
                        for i = 1, 1000 do
                            funcB(i)
                        end
                    end
                    funcC()
                ", "main.lua");

                state.lua_getglobal("prof");
                state.lua_getfield(-1, "stop");
                state.lua_pcall_traceback(0, 0);
                state.lua_pop(1); //prof

                foreach (Sample sample in _samples)
                {
                    Console.WriteLine(sample.stack + "," + sample.vmstate + ": " + sample.traceback);
                }

                Console.WriteLine("OK");
            }
        }

        private static void EvalScript(LuaState state, string script, string chunkName)
        {
            File.WriteAllText(chunkName, script);
            state.luaL_loadfile(chunkName);

            // state.luaL_loadbufferx(script, script.Length, chunkName, "t");
            // state.luaL_loadstring(script);

            state.lua_pcall_traceback(0, 0);
        }

        private static readonly List<Sample> _samples = new List<Sample>();

        private class Sample
        {
            internal readonly int samples;
            internal readonly string vmstate;
            internal readonly string stack;
            internal readonly string traceback;

            public Sample(int samples, string vmstate, string stack, string traceback)
            {
                this.samples = samples;
                this.vmstate = vmstate;
                this.stack = stack;
                this.traceback = traceback;
            }
        }

        private static int Handle(LuaState state)
        {
            int samples = state.lua_tointeger(2);
            string vmstate = state.lua_tostring(3).ManagedCopy();

            string traceback = CurrentLine() +"";

            state.lua_getglobal("prof");
            state.lua_getfield(-1, "dumpstack");
            state.lua_pushvalue(1); // thread
            state.lua_pushstring("pFZ,l;");
            state.lua_pushinteger(-100);
            state.lua_pcall_traceback(3, 1);
            string stack = state.lua_tostring(-1).ManagedCopy();

            _samples.Add(new Sample(samples, vmstate, stack, traceback));

            state.lua_pop(1); // jit_prof
            return 0;
        }

        private static int CurrentLine()
        {
            LuaState state = _currentState.Value;
            if (state.ptr == default)
            {
                throw new Exception("WTF");
            }

            state.lua_getglobal("debug");
            state.lua_getfield(-1, "getinfo");
            state.lua_pushinteger(1);
            state.lua_pushstring("l");
            state.lua_pcall_traceback(2, 1);
            if (!state.lua_istable(-1))
            {
                throw new Exception("WTF");
            }

            state.lua_getfield(-1, "currentline");
            int line = state.lua_tointeger(-1);
            return line;
        }
    }
}