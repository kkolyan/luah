using System;

namespace Luah.Test
{
    public class NestedExceptionExample
    {
        public static void Main()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();
                
                state.lua_pushcfunction(scope =>
                {
                    CountDown(3);
                    return 0;
                });
                state.lua_pcall_traceback(0, 0);
            }
        }

        private static void CountDown(int n)
        {
            if (n > 0)
            {
                CountDown(n - 1);
                return;
            }

            throw new Exception("Surprize!");
        }
    }
}