using System;

namespace Luah.Test
{
    public class TableDebugExample
    {
        public static void Main()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();
                state.eval(@"return {17, 42, -8}", 0, 1);
                state.eval(@"return {x=12, y=-42}", 0, 1);
                state.eval(@"return {17, 42, -8, x=12, y=-42}", 0, 1);
                state.eval(@"return {17, 42, -8, w={1, {4,5,6}, 2, 3}}", 0, 1);

                state.luaL_newmetatable("SuperTable");
                state.lua_pushinteger(42);
                state.lua_setfield(-2, "__main_answer");
                state.lua_pop(1);
                
                state.luaL_setmetatable("SuperTable");

                Console.WriteLine("Set breakpoint here and examine `state.Stack` for extra rendered info");
            }
        }
    }
}