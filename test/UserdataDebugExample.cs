using System;
using System.IO;
using System.Runtime.InteropServices;
using Luah.Debug;

namespace Luah.Test
{
    public class UserdataDebugExample
    {

        class MyStringWriter : StringWriter
        {
            public override string ToString()
            {
                return $"MyStringWriter({base.ToString()})";
            }
        }
        
        public static void Main()
        {
            
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();

                ReferenceBoard board = new ReferenceBoard();

                // define StringWriter metatable
                {
                    state.luaL_newmetatable("StringWriter");
                    state.lua_newtable();
                    state.lua_pushcfunction(scope =>
                    {
                        VoidPtr ptr = scope.luaL_checkudata(1, "StringWriter");
                        string arg = scope.lua_tostring(2).ManagedCopy();
                        long index = Marshal.ReadInt64(ptr.Value);
                        StringWriter writer = (StringWriter) board.Get(index);
                        writer.Write(arg);
                        return 0;
                    });
                    state.lua_setfield(-2, "write");
                    state.lua_setfield(-2, "__index");
                }

                long obj001_ref;
                // create StringWriter udata
                {
                    obj001_ref = board.Pin(new MyStringWriter());
                    VoidPtr obj001_lua = state.lua_newuserdata(sizeof(int));
                    Marshal.WriteInt64(obj001_lua.Value, obj001_ref);
                    state.luaL_getmetatable("StringWriter");
                    state.lua_setmetatable(-2);
                }

                Userdata.CurrentUserdataResolver = ptr =>
                {
                    long index = Marshal.ReadInt64(ptr.Value);
                    return board.Get(index);
                };
                
                Console.WriteLine("Set breakpoint here and examine `state.Stack` for extra rendered info");
            }
        }
    }
}