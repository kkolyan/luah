using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Luah.Test.dev
{
    public class ParseLuah
    {
        public static void Main()
        {
            List<string> statements = new List<string>();
            statements.AddRange(Generate("lauxlib.h", "LUALIB_API"));
            statements.AddRange(Generate("lualib.h", "LUALIB_API"));
            statements.AddRange(Generate("lua.h", "LUA_API"));
            WriteCs(statements);
        }
        public static List<string> Generate(string header, string marker)
        {
            List<string> statements = new List<string>();
            List<string> constants = new List<string>();
            string[] lines = File.ReadAllLines($"/e/dev/cpp/LuaJIT/src/{header}");
            string tail = null;
            foreach (string line0 in lines)
            {
                string line = line0;
                if (tail != null)
                {
                    line = tail + line.TrimStart();
                    tail = null;
                }
                if (line.StartsWith($"{marker}"))
                {
                    if (!line.EndsWith(");"))
                    {
                        tail = line;
                    }
                    else
                    {
                        string result;
                        string replacement = "$1$2$3";
                        result = new Regex($@"{marker}\s+(.*)\s*\(([A-z0-9_]+)\)\s*(\(.*\))").Replace(line, replacement);
                        if (result == line)
                        {
                            result = new Regex($@"{marker}\s+(.*)\s*([A-z0-9_]+)\s*(\(.*\))").Replace(line, replacement);
                        }

                        result = result.Replace("const char *const lst[]", "string[] lst");
                        result = new Regex(@"const char\s+\*").Replace(result, "string ");
                        result = new Regex(@"void\s+\*\*").Replace(result, "void_ptr_ptr ");
                        result = new Regex(@"const void\s+\*").Replace(result, "void_ptr ");
                        result = new Regex(@"void\s+\*").Replace(result, "void_ptr ");
                        result = new Regex(@"lua_State\s+\*").Replace(result, "lua_State_ptr ");
                        result = result.Replace("size_t", "int");
                        // result = result.Replace(" *", "_ptr ");
                        result = result.Replace("const lua_Debug *", "in lua_Debug ");
                        result = result.Replace("lua_Debug *", "ref lua_Debug ");
                        result = result.Replace("luaL_Buffer *", "ref luaL_Buffer ");
                        result = result.Replace("const lua_Number *", "double ");
                        result = result.Replace("lua_Number *", "lua_Number_ptr ");
                        result = result.Replace("lua_Integer *", "lua_Integer_ptr ");
                        result = result.Replace("lua_Number", "double");
                        result = result.Replace("lua_Integer", "int");
                        result = result.Replace("int *", "ref int ");
                        result = result.Replace("const luaL_Reg *", "luaL_Reg[] ");
                        result = result.Replace("char *", "IntPtr ");
                        result = result.Replace("(void)", "()");
                        result = result.Replace("int ref", "int @ref");
                        result = $@"
        /*
        {line}
        */
        { ((result.Contains("va_list") || result.Contains(", ...")) ? "//" : "") }[DllImport(LuaJitLibName)] public static extern {result}";

                        Console.WriteLine();

                        // Console.WriteLine(line);
                        // Console.WriteLine(result);

                        // Console.WriteLine();
                        Console.WriteLine(result);

                        statements.Add(result);
                    }
                }
                else if (line.StartsWith("#define"))
                {
                    string result = new Regex(@"#define\s+([A-z0-9_]+)\s+([0-9]+)").Replace(line, "        public const int $1 = $2;");
                    if (result != line)
                    {
                        constants.Add(result);
                    }
                }
            }
            statements.InsertRange(0, constants);
            statements.Insert(0, "");
            statements.Insert(0, $"#region {header}");
            statements.Insert(0, "");
            statements.Add($"#endregion {header}");

            return statements;
        }

        private static void WriteCs(List<string> statements)
        {
            var luaCs = "/c/dev/cs/src/src/Lua.cs";
            List<string> afterLines = new List<string>();
            afterLines.Add(@"using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;

namespace src
{
    [SuppressMessage(""ReSharper"", ""CommentTypo"")]
    [SuppressMessage(""ReSharper"", ""IdentifierTypo"")]
    [SuppressMessage(""ReSharper"", ""InconsistentNaming"")]
    public static class Lua {
        public const string LuaJitLibName = ""luajit.dll"";
");
            // bool inRegion = false;
            // foreach (string line in beforeLines)
            // {
            //     if (inRegion)
            //     {
            //         if (line.TrimStart().StartsWith("#endregion"))
            //         {
            //             inRegion = false;
            //             afterLines.Add(line);
            //         }
            //     }
            //     else
            //     {
            //         afterLines.Add(line);
            //         if (line.TrimStart().StartsWith("#region generated lua.h"))
            //         {
            //             inRegion = true;
            //             afterLines.AddRange(statements);
            //         }
            //     }
            // }
            afterLines.AddRange(statements);

            afterLines.Add("    }");
            afterLines.Add("}");
            File.WriteAllLines("/c/dev/cs/src/src/Lua.cs", afterLines);
        }
    }
}