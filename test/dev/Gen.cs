using System;
using System.Linq;
using System.Reflection;

namespace Luah.Test.dev
{
    public class Gen
    {
        private static string TranslateType(string type)
        {
            if (type.EndsWith("&"))
            {
                return "ref " + TranslateType(type.Substring(0, type.Length - 1));
            }

            return type
                .Replace("Boolean", "bool")
                .Replace("Int32", "int")
                .Replace("Void", "void")
                .Replace("String", "string");
        }

        public static void Main()
        {
            foreach (MethodInfo m in typeof(Lua).GetMethods())
            {
                string paramDecls = string.Join(", ", m.GetParameters()
                        .Select(x => $"{TranslateType(x.ParameterType.Name)} {x.Name}")
                    )
                    .Replace("lua_State_ptr L, ", "")
                    .Replace("lua_State_ptr L)", ")");
                string paramValues = string.Join(", ", m.GetParameters().Select(x => (x.ParameterType.IsByRef ? "ref " : "") + x.Name));
                Console.WriteLine($@"
        public {TranslateType(m.ReturnType.Name)} {m.Name}({paramDecls}) {{
            {(m.ReturnType == typeof(void) ? "" : "return ")}Lua.{m.Name}({paramValues});
        }}");
            }
        }
    }
}