using System;

namespace Luah.Test
{
    public class WrongNResultsExample
    {
        public static void Main()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();
                
                state.luaL_loadstring_safe("return 1, 2");
                state.lua_pcall_traceback(0, Lua.LUA_MULTRET);
                
                Console.WriteLine("Ok");
            }
        }
    }
}