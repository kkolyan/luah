﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Luah.Debug;
using Luah.Reflection;
using NUnit.Framework;

namespace Luah.Test
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void Test2x2()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_loadstring_safe("return 2 * 2");
                state.lua_pcall(0, 0);

                var result = state.luaL_optinteger(0, 0);

                Assert.AreEqual(4, result);
            }
        }

        [Test]
        public void TestCallFunc()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();
                state.luaL_loadstring_safe(@"
                    return function (a, b)  
                        return a * b
                    end
                ");
                state.lua_pcall(0, 1);
                state.lua_pushnumber(2);
                state.lua_pushnumber(2);
                state.lua_pcall(2, 1);
                var result = state.luaL_optinteger(-1, 0);

                Assert.AreEqual(4, result);
            }
        }

        [Test]
        public void TestCallback()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();
                state.luaL_loadstring_safe(@"return function(n, callback)
                    local c = 0;
                    for i = 1, n do
                        c = callback(c, i)                    
                    end
                    return c
                end");
                state.lua_pcall(0, 1);

                state.lua_pushinteger(3);
                state.lua_pushcfunction(scope =>
                {
                    int c = scope.luaL_checkinteger(1);
                    int i = scope.luaL_checkinteger(2);
                    scope.lua_pushinteger(c + i);
                    return 1;
                });

                state.lua_pcall(2, 1);

                Assert.AreEqual(1 + 2 + 3, state.luaL_optinteger(1, 0));
            }
        }

        [Test]
        public void TestManagedObject()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();


                ReferenceBoard board = new ReferenceBoard();

                // define StringWriter metatable
                {
                    state.luaL_newmetatable("StringWriter");
                    state.lua_newtable();
                    state.lua_pushcfunction(scope =>
                    {
                        VoidPtr ptr = scope.luaL_checkudata(1, "StringWriter");
                        string arg = scope.lua_tostring(2).ManagedCopy();
                        long index = Marshal.ReadInt64(ptr.Value);
                        StringWriter writer = (StringWriter) board.Get(index);
                        writer.Write(arg);
                        return 0;
                    });
                    state.lua_setfield(-2, "write");
                    state.lua_setfield(-2, "__index");
                }

                state.luaL_loadstring_safe(@"return function(writer)
                    writer:write('Hello! ')
                    writer:write(42)
                end");

                state.lua_pcall(0, 1);

                long obj001_ref;
                // create StringWriter udata
                {
                    obj001_ref = board.Pin(new StringWriter());
                    VoidPtr obj001_lua = state.lua_newuserdata(sizeof(int));
                    Marshal.WriteInt64(obj001_lua.Value, obj001_ref);
                    state.luaL_getmetatable("StringWriter");
                    state.lua_setmetatable(-2);
                }
                state.lua_pcall_traceback(1, 0);

                Assert.AreEqual("Hello! 42", board.Get(obj001_ref).ToString());
            }
        }

        [Test]
        public void TestManagedObjectGc()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();

                ReferenceBoard board = new ReferenceBoard();

                // define StringWriter metatable
                {
                    state.luaL_newmetatable("StringWriter");
                    state.lua_pushcfunction(scope =>
                    {
                        VoidPtr ptr = scope.luaL_checkudata(1, "StringWriter");
                        long index = Marshal.ReadInt64(ptr.Value);
                        board.Unpin(index);
                        return 0;
                    });
                    state.lua_setfield(-2, "__gc");
                    state.lua_pop(1);
                }

                long obj001_ref;
                // create StringWriter udata
                {
                    obj001_ref = board.Pin(new StringWriter());
                    VoidPtr obj001_lua = state.lua_newuserdata(sizeof(int));
                    Marshal.WriteInt64(obj001_lua.Value, obj001_ref);
                    state.luaL_setmetatable("StringWriter");
                }
                Assert.True(board.Exists(obj001_ref));
                state.lua_pop(1);
                state.lua_gc(Lua.LUA_GCCOLLECT, 0);

                Assert.False(board.Exists(obj001_ref));
            }
        }

        [Test]
        public void TestStackTrace()
        {
            try
            {
                using (LuaState state = Lua.luaL_newstate())
                {
                    state.luaL_openlibs();

                    state.luaL_loadstring_safe(@"
                        function func001(m)
                            test_error(m)
                        end
                        
                        function func002(m)
                            func001(m)
                        end
                        
                        func002('hi')
                    ");
                    state.lua_pcall_traceback(0, 0);
                }

                Assert.Fail("Exception expected");
            }
            catch (LuaException e)
            {
                Assert.AreEqual("lua_pcall: a runtime error: [string \"...\"]:3: attempt to call global 'test_error' (a nil value)\n" +
                                "stack traceback:\n" +
                                "\t[string \"...\"]:3: in function 'func001'\n" +
                                "\t[string \"...\"]:7: in function 'func002'\n" +
                                "\t[string \"...\"]:10: in main chunk", e.Message);
            }
        }

        [Test]
        public void TestDebug()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();
                state.eval(@"return {17, 42, -8}", 0, 1);
                state.eval(@"return {x=12, y=-42}", 0, 1);
                state.eval(@"return {16, 37, -8, x=12, y=-42}", 0, 1);
                state.eval(@"return {18, 43, -8, w={99, {4,5,6}, 2, 3}}", 0, 1);

                Table t1 = (Table) new LuaStateDebugView(state).Stack[3];
                List<ILuaIPair> t1is = t1.IndexedPairs.ToList();
                Assert.AreEqual(new Pair(1, new NumberValue(18)).ToString(), t1is[0].ToString());
                Assert.AreEqual(new Pair(2, new NumberValue(43)).ToString(), t1is[1].ToString());

                Table t2 = (Table) t1.NamedPairs.Single(x => x.Key == "w").Value;
                Assert.AreEqual(new Pair(1, new NumberValue(99)).ToString(), t2.IndexedPairs.First().ToString());

                Table t3 = (Table) t2.IndexedPairs.Single(x => x.Index == 2).Value;
                Assert.AreEqual(new NumberValue(5), t3.IndexedPairs.Single(x => x.Index == 2).Value);
            }
        }

        [Test]
        public void UserdataPropertyReadViaMetatable()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                Dictionary<IntPtr, int> ages = new Dictionary<IntPtr, int>();
                state.luaL_openlibs();
                state.luaL_newmetatable("Pet");
                state.lua_pushcfunction(scope =>
                {
                    VoidPtr self = scope.luaL_checkudata(1, "Pet");
                    string property = scope.luaL_checkstring(2).ManagedCopy();
                    if (property == "age")
                    {
                        scope.lua_pushinteger(ages[self.Value]);
                    }
                    else
                    {
                        scope.lua_pushnil();
                    }

                    return 1;
                });
                state.lua_setfield(-2, "__index");

                VoidPtr pet1 = state.lua_newuserdata(sizeof(long));
                ages[pet1.Value] = 42;

                state.luaL_setmetatable("Pet");

                state.lua_setglobal("pet1");
                state.eval("return pet1.age", 0, 1);

                Assert.AreEqual(42, state.lua_tointeger(-1));
            }
        }

        [Test]
        public void UserdataPropertyWriteViaMetatable()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                Dictionary<IntPtr, int> ages = new Dictionary<IntPtr, int>();
                state.luaL_openlibs();
                state.luaL_newmetatable("Pet");
                state.lua_pushcfunction(scope =>
                {
                    VoidPtr self = scope.luaL_checkudata(1, "Pet");
                    string property = scope.luaL_checkstring(2).ManagedCopy();
                    if (property == "age")
                    {
                        ages[self.Value] = scope.luaL_checkinteger(3);
                        return 0;
                    }

                    scope.lua_pushstring($"unknown property: {property}");
                    return scope.lua_error();
                });
                state.lua_setfield(-2, "__newindex");

                VoidPtr pet1 = state.lua_newuserdata(sizeof(long));

                state.luaL_setmetatable("Pet");

                state.lua_setglobal("pet1");
                state.eval("pet1.age = 42", 0, 1);

                Assert.AreEqual(42, ages[pet1.Value]);

                state.eval("pet1.age = 17", 0, 1);
                Assert.AreEqual(17, ages[pet1.Value]);
            }
        }

        [Test]
        public void UserdataAsModule()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();

                state.luaL_newmetatable("Pet");
                state.lua_pushcfunction(scope =>
                {
                    VoidPtr self = scope.luaL_checkudata(1, "Pet");
                    string property = scope.luaL_checkstring(2).ManagedCopy();
                    if (property == "age")
                    {
                        scope.lua_pushinteger(42);
                    }
                    else
                    {
                        scope.lua_pushnil();
                    }

                    return 1;
                });
                state.lua_setfield(-2, "__index");

                state.lua_pop(1); // metatable

                state.lua_getglobal("package");
                state.lua_getfield(-1, "loaded");

                state.lua_newuserdata(sizeof(long));
                state.luaL_setmetatable("Pet");

                state.lua_setfield(-2, "pet_module");
                state.lua_pop(2); //package.loaded

                state.eval("return require('pet_module').age", 0, 1);
                Assert.AreEqual(42, state.lua_tointeger(-1));
            }
        }

        [Test]
        public void LuaStringOverloadWorks()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();
                state.lua_pushcfunction(scope =>
                {
                    scope.lua_pushstring("hello!");
                    int luaError = scope.luaL_error("Hello Error!");
                    return luaError;
                });
                state.lua_setglobal("hello");
                try
                {
                    state.eval("hello()", 0, 0);
                    Assert.Fail("Exception expected!");
                }
                catch (LuaException e)
                {
                    Assert.AreEqual(@"lua_pcall: a runtime error: [string ""hello()""]:1: Hello Error!
                        stack traceback:
                            [C]: in function 'hello'
                            [string ""hello()""]:1: in main chunk".Replace("                        ", ""), e.Message.Replace("\t", "    "));
                }
            }
        }
    }
}