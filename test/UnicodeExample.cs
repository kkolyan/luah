using System.IO;
using System.Text;
using NUnit.Framework;

namespace Luah.Test
{
    public class UnicodeExample
    {
        public static void Main()
        {
            using (LuaState state = Lua.luaL_newstate())
            {
                state.luaL_openlibs();

                const string unicodeSampeFile = "unicode_sampe.lua";
                // File.WriteAllText(unicodeSampeFile, "return 'Лыжи'", Encoding.UTF8);
                
                state.luaL_loadfile_safe(unicodeSampeFile);
                state.lua_pcall_traceback(0, 1);
                string managedCopy = state.lua_tostring(-1).ManagedCopy(true);
                Assert.AreEqual("Лыжи", managedCopy);
            }
        }
    }
}