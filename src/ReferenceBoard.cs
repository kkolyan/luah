using System;
using System.Collections.Generic;
using System.Linq;

namespace Luah
{
    public class ReferenceBoard
    {
        private IDictionary<long, Place> _references = new Dictionary<long, Place>();
        private long _nextId = 1;

        private struct Place
        {
            internal object reference;
            internal Action onUnpin;
        }

        public long Pin<T>(T reference, Action<T> onUnpin = null) where T : class
        {
            long index = _nextId++;
            _references[index] = new Place
            {
                reference = reference,
                onUnpin = onUnpin == null ? (Action) null : () => onUnpin(reference)
            };
            return index;
        }

        public object Get(long pinnedPtr)
        {
            return _references[pinnedPtr].reference;
        }

        public void Unpin(long pinnedPtr)
        {
            _references[pinnedPtr].onUnpin?.Invoke();
            _references.Remove(pinnedPtr);
        }

        public bool Exists(long pinnedPtr)
        {
            return _references.ContainsKey(pinnedPtr);
        }

        public void Clear()
        {
            foreach (KeyValuePair<long, Place> place in _references.ToList())
            {
                place.Value.onUnpin?.Invoke();
                _references.Remove(place.Key);
            }

            System.Diagnostics.Debug.Assert(_references.Count == 0, "_references.Count == 0");
            _nextId = 1;
        }
    }
}