using System;
using System.Runtime.InteropServices;

namespace Luah
{
    public struct CString
    {
        public IntPtr Ptr;

        public override string ToString()
        {
            return $"0x{Ptr.ToInt64():x}";
        }

        public string ManagedCopy(bool unicode = false)
        {
            return unicode
                ? Marshal.PtrToStringUni(Ptr)
                : Marshal.PtrToStringAnsi(Ptr);
        }

        public string ManagedCopyValue => ManagedCopy();
    }
}