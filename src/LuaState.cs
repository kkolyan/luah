using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using Luah.Debug;
using Luah.Reflection;

namespace Luah
{
    [DebuggerTypeProxy(typeof(LuaStateDebugView))]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [Serializable]
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    [SuppressMessage("ReSharper", "CommentTypo")]
    public struct LuaState : IDisposable
    {
        public IntPtr ptr;

        public void Dispose()
        {
            Lua.lua_close(this);
        }

        private static string TranslateCode(int code)
        {
            if (code == Lua.LUA_ERRSYNTAX)
            {
                return "syntax error during pre-compilation";
            }

            if (code == Lua.LUA_ERRMEM)
            {
                return "memory allocation error";
            }

            if (code == Lua.LUA_ERRRUN)
            {
                return "a runtime error";
            }

            return $"error {code}";
        }

        public void TranslateResult(int resultCode, string call)
        {
            if (resultCode != Lua.LUA_OK)
            {
                string translatedCode = TranslateCode(resultCode);
                throw ThrowLuaException(call, translatedCode);
            }
        }

        private LuaException ThrowLuaException(string call, string translatedCode, Exception cause = null)
        {
            string errorMessage = this.lua_tostring(-1).ManagedCopy();
            return new LuaException($"{call}: {translatedCode}: {errorMessage}", cause);
        }

        public void eval(string s, int nargs, int nresults)
        {
            luaL_loadstring_safe(s);
            lua_pcall_traceback(nargs, nresults);
        }

        public void luaL_loadstring_safe(string s)
        {
            TranslateResult(this.luaL_loadstring(s), "luaL_loadstring");
        }

        /**
         * from LuaJit as is
         */
        private static int traceback(LuaState L)
        {
            if (!L.lua_isstring(1))
            {
                /* Non-string error object? Try metamethod. */
                if (L.lua_isnoneornil(1) ||
                    !L.luaL_callmeta(1, "__tostring") ||
                    !L.lua_isstring(-1))
                    return 1; /* Return non-string error object. */
                L.lua_remove(1); /* Replace object by result of __tostring metamethod. */
            }

            L.luaL_traceback(L, L.lua_tostring(1), 1);
            return 1;
        }

        public void lua_pcall_traceback(int nargs, int nresults)
        {
            this.lua_pushcfunction(traceback);
            int errorHandlerIdx = -1 /* pseudo index of the stack top */ - nargs - 1 /* to offset the original function*/;
            this.lua_insert(errorHandlerIdx);
            // int errorHandlerIdx = 0;

            int before = this.lua_gettop();
            int luaPcall;
            try
            {
                luaPcall = this.lua_pcall(nargs, nresults, errorHandlerIdx);
            }
            /*
             In rare cases  it just fails with NRE:
                NullReferenceException: Object reference not set to an instance of an object
                Luah.LuaFunctionsAsExtensions.lua_pcall (Luah.LuaState thiz, System.Int32 nargs, System.Int32 nresults, System.Int32 errfunc) (at Library/PackageCache/com.nplekhanov.csx.luah@c7396a92d2/src/LuaFunctionsAsExtensions.cs:139)
                Luah.LuaState.lua_pcall_traceback (System.Int32 nargs, System.Int32 nresults) (at Library/PackageCache/com.nplekhanov.csx.luah@c7396a92d2/src/LuaState.cs:96)
                
             Don't know precise scenario and reasons. maybe some weird effect of longjmp
            */
            catch (NullReferenceException e)
            {
                throw ThrowLuaException("lua_pcall", "-1", e);
            }

            int after = this.lua_gettop();
            
            TranslateResult(luaPcall, "lua_pcall");
            int functionAndArgs = nargs + 1;
            int someShit = before - functionAndArgs;
            int actualResults = after - someShit;
            this.lua_remove(-1 - actualResults);// traceback
        }

        public void luaL_loadfile_safe(string file)
        {
            TranslateResult(this.luaL_loadfile(file), "luaL_loadfile_safe");
        }

        public ILuaState Introspect()
        {
            return new LuaStateDebugView(this);
        }
    }
}