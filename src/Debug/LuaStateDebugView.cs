using System;
using System.Collections.Generic;
using Luah.Reflection;

namespace Luah.Debug
{
    public sealed class LuaStateDebugView : ILuaState
    {
        private readonly LuaState _state;

        public LuaStateDebugView(LuaState state)
        {
            _state = state;
        }

        public ILuaTable Globals
        {
            get
            {
                _state.lua_pushvalue(Lua.LUA_GLOBALSINDEX);
                VoidPtr ptr = _state.lua_topointer(-1);
                _state.lua_pop(1);
                return new Table(_state, ptr, new[] {new PathSegment(PathSegment.Type.Global)});
            }
        }

        public List<ILuaValue> Stack
        {
            get
            {
                int top = Lua.lua_gettop(_state);

                List<ILuaValue> elements = new List<ILuaValue>();

                for (int i = 1; i <= top; i++)
                {
                    elements.Add(ToClrValue(_state, i, new[] {new PathSegment(PathSegment.Type.StackIndex, i)}));
                }

                return elements;
            }
        }


        internal static PathSegment[] Combine(PathSegment[] path, PathSegment element)
        {
            PathSegment[] result = new PathSegment[path.Length + 1];
            Array.Copy(path, result, path.Length);
            result[result.Length - 1] = element;
            return result;
        }

        // "base" means the state of stack when Stack property invocation started
        internal static ILuaValue ToClrValue(LuaState state, int currentIndex, PathSegment[] pathFromBase)
        {
            int luaType = state.lua_type(currentIndex);
            switch (luaType)
            {
                case Lua.LUA_TNUMBER:
                    return new NumberValue(state.lua_tonumber(currentIndex));
                case Lua.LUA_TSTRING:
                    return new StringValue(state.lua_tostring(currentIndex).ManagedCopy());
                case Lua.LUA_TBOOLEAN:
                    return new BoolValue(state.lua_toboolean(currentIndex));
                case Lua.LUA_TNIL:
                case Lua.LUA_TNONE:
                    return NilValue.Instance;
                case Lua.LUA_TTABLE:
                    return new Table(state, state.lua_topointer(currentIndex), pathFromBase);
                case Lua.LUA_TFUNCTION:
                    return new Function(state.lua_topointer(currentIndex), state.lua_iscfunction(currentIndex));
                case Lua.LUA_TUSERDATA:
                    VoidPtr data = state.lua_touserdata(currentIndex);
                    Table metatable;
                    if (state.lua_getmetatable(currentIndex))
                    {
                        metatable = new Table(state, state.lua_topointer(-1), Combine(pathFromBase, new PathSegment(PathSegment.Type.MetaTable)));
                        state.lua_pop(1);
                    }
                    else
                    {
                        metatable = null;
                    }

                    return new Userdata(data, metatable);
                default:
                    return new ObfuscatedLuaValue(state.lua_typename(luaType).ManagedCopy(), state.lua_topointer(currentIndex), luaType);
            }
        }
    }
}