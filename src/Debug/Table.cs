using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Luah.Reflection;

namespace Luah.Debug
{
    [DebuggerTypeProxy(typeof(MinimalisticTableDebugView))]
    public class Table : LuaValueBase, ILuaTable
    {
        private readonly LuaState _state;
        private readonly PathSegment[] _address;
        private readonly VoidPtr _ptr;

        public Table(LuaState state, VoidPtr ptr, PathSegment[] address)
        {
            _state = state;
            _ptr = ptr;
            _address = address;
        }

        public ILuaTable Metatable
        {
            get
            {
                PushExtraPath();

                Table mt = _state.lua_getmetatable(-1)
                    ? new Table(_state, _state.lua_topointer(-1), LuaStateDebugView.Combine(_address, new PathSegment(PathSegment.Type.MetaTable)))
                    : null;
                PopExtraPath();
                return mt;
            }
        }

        public IEnumerable<ILuaPair> Items => LazyOrNot(() =>
        {
            List<ILuaPair> items = new List<ILuaPair>();

            PushExtraPath();

            int iterableIndex = _state.lua_gettop();

            _state.lua_pushnil();
            while (_state.lua_next(iterableIndex))
            {
                bool isnumber = _state.lua_isnumber(-2);
                PathSegment key = isnumber
                    ? new PathSegment(PathSegment.Type.TableIndex, _state.lua_tointeger(-2))
                    : new PathSegment(_state.lua_tostring(-2).ManagedCopy());

                ILuaValue value = LuaStateDebugView.ToClrValue(_state, -1, LuaStateDebugView.Combine(_address, key));
                items.Add(isnumber
                    ? new Pair(key.index, value)
                    : new Pair(key.tableKey, value));

                _state.lua_pop(1);
            }

            PopExtraPath();
            return items
                .OrderBy(pair => pair.Indexed ? 0 : 1)
                .ThenBy(pair =>  pair.Indexed ? pair.AsIndexed().Index : (object)pair.AsNamed().Key)
                .ToList();
        });

        public IEnumerable<ILuaKPair> NamedPairs => LazyOrNot(() => Items.Where(i => i.Named).Cast<ILuaKPair>().ToList());
        public IEnumerable<ILuaIPair> IndexedPairs => LazyOrNot(() => Items.Where(i => i.Indexed).Cast<ILuaIPair>().ToList());

        public ILuaValue this[int index] => IndexedPairs.FirstOrDefault(x => x.Index == index)?.Value ?? NilValue.Instance;

        public ILuaValue this[string key] => NamedPairs.FirstOrDefault(x => x.Key == key)?.Value ?? NilValue.Instance;

        public override string ToString()
        {
            return $"table({_ptr})";
        }

        private void PushExtraPath()
        {
            foreach (PathSegment o in _address)
            {
                switch (o.type)
                {
                    case PathSegment.Type.TableIndex:
                        _state.lua_rawgeti(-1, o.index);
                        break;
                    case PathSegment.Type.TableKey:
                        _state.lua_getfield(-1, o.tableKey);
                        break;
                    case PathSegment.Type.MetaTable:
                        _state.lua_getmetatable(-1);
                        break;
                    case PathSegment.Type.StackIndex:
                        _state.lua_pushvalue(o.index);
                        break;
                    case PathSegment.Type.Global:
                        _state.lua_pushvalue(Lua.LUA_GLOBALSINDEX);
                        break;
                    default:
                        throw new Exception($"unsupported index type: {o.GetType()} ({o})");
                }
            }
        }

        private void PopExtraPath()
        {
            _state.lua_pop(_address.Length);
        }

        internal static IEnumerable<T> LazyOrNot<T>(Func<IEnumerable<T>> func)
        {
            // toggle it for debugging this particular code
            if (true)
            {
                return func();
            }

            return new LazyList<T>(func);
        }

        public override ILuaTable AsTable() => this;

        public override bool IsTable() => true;
    }
}