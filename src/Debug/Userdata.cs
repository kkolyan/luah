using System;
using Luah.Reflection;

namespace Luah.Debug
{
    public class Userdata : LuaValueBase, ILuaUserdata
    {
        public static Func<VoidPtr, object> CurrentUserdataResolver;
        
        public readonly VoidPtr data;
        public readonly Table metatable;

        public Userdata(VoidPtr data, Table metatable)
        {
            this.data = data;
            this.metatable = metatable;
        }

        public override string ToString()
        {
            string extra;
            try
            {
                object resolved = Resolved;
                extra = $": ({resolved?.GetType()}) {resolved}";
            }
            catch (Exception ex)
            {
                extra = $": failed to fetch extra info: {ex}";
            }
            return $"userdata({data}){extra}";
        }
        
        public object Resolved => CurrentUserdataResolver?.Invoke(data);
        public ILuaTable Metatable => metatable;

        public override ILuaUserdata AsUserdata() => this;

        public override bool IsUserdata() => true;
    }
}