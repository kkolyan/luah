using System;

namespace Luah.Debug
{
    public class NilValue: LuaValueBase, IEquatable<NilValue>
    {
        public static readonly NilValue Instance = new NilValue();

        public override bool IsNil()
        {
            return true;
        }

        public override string ToString()
        {
            return "nil";
        }

        public bool Equals(NilValue other)
        {
            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((NilValue) obj);
        }

        public override int GetHashCode()
        {
            return 0;
        }
    }
}