using System;

namespace Luah.Debug
{
    public class BoolValue: LuaValueBase, IEquatable<BoolValue>
    {
        public readonly bool Value;

        public static readonly BoolValue True = new BoolValue(true);
        public static readonly BoolValue False = new BoolValue(false);

        public BoolValue(bool value) => Value = value;

        public override bool AsBool() => Value;

        public override bool IsBool() => true;

        public override string ToString()
        {
            return $"{Value}";
        }

        public bool Equals(BoolValue other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((BoolValue) obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}