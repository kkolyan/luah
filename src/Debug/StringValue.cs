using System;

namespace Luah.Debug
{
    public class StringValue: LuaValueBase, IEquatable<StringValue>
    {
        public readonly string Value;

        public StringValue(string value)
        {
            Value = value;
        }

        public override string AsString() => Value;

        public override bool IsString() => true;

        public override string ToString()
        {
            return $"\"{Value.Replace("\"", "\\\"")}\"";
        }

        public bool Equals(StringValue other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((StringValue) obj);
        }

        public override int GetHashCode()
        {
            return (Value != null ? Value.GetHashCode() : 0);
        }
    }
}