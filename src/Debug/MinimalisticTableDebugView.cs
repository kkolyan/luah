using System.Diagnostics;
using System.Linq;
using Luah.Reflection;

namespace Luah.Debug
{
    internal sealed class MinimalisticTableDebugView
    {
        private Table table;

        public MinimalisticTableDebugView(Table table)
        {
            this.table = table;
        }

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public ILuaPair[] Items => table.Items.ToArray();
        
        public object Metatable => table.Metatable;
    }
}