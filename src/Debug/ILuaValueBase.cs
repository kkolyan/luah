using System;
using Luah.Reflection;

namespace Luah.Debug
{
    public abstract class LuaValueBase : ILuaValue
    {
        public virtual string AsString() => throw new InvalidCastException();
        public virtual double AsNumber() => throw new InvalidCastException();
        public virtual bool AsBool() => throw new InvalidCastException();
        public virtual ILuaUserdata AsUserdata() => throw new InvalidCastException();
        public virtual ILuaTable AsTable() => throw new InvalidCastException();
        public virtual ILuaFunction AsFunction() => throw new InvalidCastException();
        public virtual bool IsNil() => false;
        public virtual bool IsString() => false;
        public virtual bool IsNumber() => false;
        public virtual bool IsBool() => false;
        public virtual bool IsUserdata() => false;
        public virtual bool IsTable() => false;
        public virtual bool IsFunction() => false;
        public virtual bool IsLightUserdata() => false;
        public virtual bool IsThread() => false;
    }
}