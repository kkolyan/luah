using System;
using System.Linq;
using System.Reflection;
using Luah.Reflection;

namespace Luah.Debug
{
    public class Function : LuaValueBase, ILuaFunction
    {
        public static Func<VoidPtr, Delegate> CurrentFunctionResolver;
        public readonly VoidPtr ptr;
        private readonly bool c;

        public Function(VoidPtr ptr, bool c)
        {
            this.ptr = ptr;
            this.c = c;
        }

        public override string ToString()
        {
            string extra;
            if (c)
            {
                MethodInfo method = Method;
                if (method == null)
                {
                    extra = ", C or unknown C#";
                }
                else
                {
                    extra =
                        $", {method.DeclaringType?.FullName}.{method.Name}({string.Join(", ", method.GetParameters().Select(param => param.ParameterType.FullName))})";
                }
            }
            else
            {
                extra = ", Lua";
            }

            return $"function({ptr}{extra})";
        }

        public MethodInfo Method => Delegate?.Method;

        public Delegate Delegate => CurrentFunctionResolver?.Invoke(ptr);

        public override ILuaFunction AsFunction() => this;

        public override bool IsFunction() => true;
    }
}