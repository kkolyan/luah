namespace Luah.Debug
{
    public class PairDebugView
    {
        private readonly Pair _pair;

        public PairDebugView(Pair pair)
        {
            _pair = pair;
        }

        public object Key => _pair.Indexed ? _pair.Index : (object) _pair.Key;

        public object Value => _pair.Value;
    }
}