using System;
using System.Diagnostics;
using Luah.Reflection;

namespace Luah.Debug
{
    [DebuggerTypeProxy(typeof(PairDebugView))]
    public readonly struct Pair : ILuaIPair, ILuaKPair
    {
        private enum PairType
        {
            Indexed,
            Named,
        }
        
        private PairType Type { get; }
        public int Index { get; }
        public string Key { get; }
        public bool Named => Type == PairType.Named;
        public bool Indexed => Type == PairType.Indexed;
        public ILuaValue Value { get; }
        
        public ILuaIPair AsIndexed()
        {
            if (Type == PairType.Indexed)
            {
                return this;
            }

            throw new InvalidCastException();
        }

        public ILuaKPair AsNamed()
        {
            if (Type == PairType.Named)
            {
                return this;
            }

            throw new InvalidCastException();
        }

        public Pair(int index, ILuaValue value) : this()
        {
            Index = index;
            Value = value;
            Type = PairType.Indexed;
        }

        public Pair(string key, ILuaValue value) : this()
        {
            Key = key;
            Value = value;
            Type = PairType.Named;
        }

        public override string ToString()
        {
            string value;
            try
            {
                value = Value?.ToString();
            }
            catch (Exception e)
            {
                value = e.ToString();
            }
            return $"{(Type == PairType.Indexed ? Index.ToString() : $"\"{Key.Replace("\"", "\\\"")}\"")}: {value}";
        }
    }
}