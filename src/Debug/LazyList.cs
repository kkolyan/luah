using System;
using System.Collections;
using System.Collections.Generic;

namespace Luah.Debug
{
    internal class LazyList<T> : IEnumerable<T>
    {
        private readonly Func<IEnumerable<T>> _list;
        public LazyList(Func<IEnumerable<T>> list) => _list = list;
        public IEnumerator<T> GetEnumerator() => _list().GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}