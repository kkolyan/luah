namespace Luah.Debug
{
    internal class ObfuscatedLuaValue : LuaValueBase
    {
        private readonly string _luaType;
        private readonly VoidPtr _raw;
        private readonly int _type;

        public ObfuscatedLuaValue(string luaType, VoidPtr raw, int type)
        {
            _luaType = luaType;
            _raw = raw;
            _type = type;
        }

        public override string ToString()
        {
            return $"{_luaType}({_raw})";
        }

        public override bool IsLightUserdata() => _type == Lua.LUA_TLIGHTUSERDATA;

        public override bool IsThread() => _type == Lua.LUA_TTHREAD;
    }
}