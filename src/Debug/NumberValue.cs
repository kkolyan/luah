using System;
using System.Collections.Generic;

namespace Luah.Debug
{
    public class NumberValue: LuaValueBase, IEquatable<NumberValue>
    {
        public readonly double Value;

        public NumberValue(double value)
        {
            Value = value;
        }

        public override double AsNumber()
        {
            return Value;
        }

        public override bool IsNumber()
        {
            return true;
        }

        public override string ToString()
        {
            return $"{Value}";
        }


        public bool Equals(NumberValue other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Value.Equals(other.Value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((NumberValue) obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}