namespace Luah.Debug
{
    public readonly struct PathSegment
    {
        public enum Type
        {
            TableKey,
            Global,
            MetaTable,
            TableIndex,
            StackIndex,
        }

        public readonly Type type;
        public readonly string tableKey;
        public readonly int index;

        public PathSegment(string tableKey) : this()
        {
            type = Type.TableKey;
            this.tableKey = tableKey;
        }

        public PathSegment(Type type, int index) : this()
        {
            this.type = type;
            this.index = index;
        }

        public PathSegment(Type type) : this()
        {
            this.type = type;
        }
    }
}