using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;

namespace Luah
{
    [SuppressMessage("ReSharper", "CommentTypo")]
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class Lua {
        
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            public const string LuaJitLibName = "./lua51.dll";
#elif UNITY_STANDALONE_LINUX || UNITY_STANDALONE_LINUX
            public const string LuaJitLibName = "./liblua51.so";
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            public const string LuaJitLibName = "./liblua51-osx.so";
#else 
        // default to windows :)
        //  actually on my standalone Ubuntu it resolves to liblua51.so, but on Ubuntu/WSL2 it doesn't.
        public const string LuaJitLibName = "./lua51.dll";
#endif


#region lauxlib.h


        /*
        LUALIB_API void (luaL_openlib) (lua_State *L, const char *libname,const luaL_Reg *l, int nup);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_openlib(LuaState L, string libname,LuaL_Reg[] l, int nup);

        /*
        LUALIB_API void (luaL_register) (lua_State *L, const char *libname,const luaL_Reg *l);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_register(LuaState L, string libname,LuaL_Reg[] l);

        /*
        LUALIB_API int (luaL_getmetafield) (lua_State *L, int obj, const char *e);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_getmetafield(LuaState L, int obj, string e);

        /*
        LUALIB_API int (luaL_callmeta) (lua_State *L, int obj, const char *e);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_callmeta(LuaState L, int obj, string e);

        /*
        LUALIB_API int (luaL_typerror) (lua_State *L, int narg, const char *tname);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_typerror(LuaState L, int narg, string tname);

        /*
        LUALIB_API int (luaL_argerror) (lua_State *L, int numarg, const char *extramsg);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_argerror(LuaState L, int numarg, string extramsg);

        /*
        LUALIB_API const char *(luaL_checklstring) (lua_State *L, int numArg,size_t *l);
        */
        [DllImport(LuaJitLibName)] public static extern CString luaL_checklstring(LuaState L, int numArg, IntPtr l);

        /*
        LUALIB_API const char *(luaL_optlstring) (lua_State *L, int numArg,const char *def, size_t *l);
        */
        [DllImport(LuaJitLibName)] public static extern CString luaL_optlstring(LuaState L, int numArg,string def, IntPtr l);

        /*
        LUALIB_API lua_Number (luaL_checknumber) (lua_State *L, int numArg);
        */
        [DllImport(LuaJitLibName)] public static extern double luaL_checknumber(LuaState L, int numArg);

        /*
        LUALIB_API lua_Number (luaL_optnumber) (lua_State *L, int nArg, lua_Number def);
        */
        [DllImport(LuaJitLibName)] public static extern double luaL_optnumber(LuaState L, int nArg, double def);

        /*
        LUALIB_API lua_Integer (luaL_checkinteger) (lua_State *L, int numArg);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_checkinteger(LuaState L, int numArg);

        /*
        LUALIB_API lua_Integer (luaL_optinteger) (lua_State *L, int nArg,lua_Integer def);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_optinteger(LuaState L, int nArg,int def);

        /*
        LUALIB_API void (luaL_checkstack) (lua_State *L, int sz, const char *msg);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_checkstack(LuaState L, int sz, string msg);

        /*
        LUALIB_API void (luaL_checktype) (lua_State *L, int narg, int t);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_checktype(LuaState L, int narg, int t);

        /*
        LUALIB_API void (luaL_checkany) (lua_State *L, int narg);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_checkany(LuaState L, int narg);

        /*
        LUALIB_API int   (luaL_newmetatable) (lua_State *L, const char *tname);
        */
        [DllImport(LuaJitLibName)] public static extern int   luaL_newmetatable(LuaState L, string tname);

        /*
        LUALIB_API void *(luaL_checkudata) (lua_State *L, int ud, const char *tname);
        */
        [DllImport(LuaJitLibName)] public static extern VoidPtr luaL_checkudata(LuaState L, int ud, string tname);

        /*
        LUALIB_API void (luaL_where) (lua_State *L, int lvl);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_where(LuaState L, int lvl);

        /*
        LUALIB_API int (luaL_error) (lua_State *L, const char *fmt, ...);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_error(LuaState L, string fmt);

        /*
        LUALIB_API int (luaL_checkoption) (lua_State *L, int narg, const char *def,const char *const lst[]);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_checkoption(LuaState L, int narg, string def,string[] lst);

        /*
        LUALIB_API int (luaL_ref) (lua_State *L, int t);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_ref(LuaState L, int t);

        /*
        LUALIB_API void (luaL_unref) (lua_State *L, int t, int ref);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_unref(LuaState L, int t, int @ref);

        /*
        LUALIB_API int (luaL_loadfile) (lua_State *L, const char *filename);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_loadfile(LuaState L, string filename);

        /*
        LUALIB_API int (luaL_loadbuffer) (lua_State *L, const char *buff, size_t sz,const char *name);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_loadbuffer(LuaState L, string buff, int sz,string name);

        /*
        LUALIB_API int (luaL_loadstring) (lua_State *L, const char *s);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_loadstring(LuaState L, string s);

        /*
        LUALIB_API lua_State *(luaL_newstate) (void);
        */
        [DllImport(LuaJitLibName)] public static extern LuaState luaL_newstate();

        /*
        LUALIB_API const char *(luaL_gsub) (lua_State *L, const char *s, const char *p,const char *r);
        */
        [DllImport(LuaJitLibName)] public static extern string luaL_gsub(LuaState L, string s, string p,string r);

        /*
        LUALIB_API const char *(luaL_findtable) (lua_State *L, int idx,const char *fname, int szhint);
        */
        [DllImport(LuaJitLibName)] public static extern string luaL_findtable(LuaState L, int idx,string fname, int szhint);

        /*
        LUALIB_API int luaL_fileresult(lua_State *L, int stat, const char *fname);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_fileresult(LuaState L, int stat, string fname);

        /*
        LUALIB_API int luaL_execresult(lua_State *L, int stat);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_execresult(LuaState L, int stat);

        /*
        LUALIB_API int (luaL_loadfilex) (lua_State *L, const char *filename,const char *mode);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_loadfilex(LuaState L, string filename,string mode);

        /*
        LUALIB_API int (luaL_loadbufferx) (lua_State *L, const char *buff, size_t sz,const char *name, const char *mode);
        */
        [DllImport(LuaJitLibName)] public static extern int luaL_loadbufferx(LuaState L, string buff, int sz,string name, string mode);

        /*
        LUALIB_API void luaL_traceback (lua_State *L, lua_State *L1, const char *msg,int level);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_traceback(LuaState L, LuaState L1, string msg, int level);
        
        
        [DllImport(LuaJitLibName)] public static extern void luaL_traceback(LuaState L, LuaState L1, CString msg, int level);

        /*
        LUALIB_API void (luaL_setfuncs) (lua_State *L, const luaL_Reg *l, int nup);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_setfuncs(LuaState L, LuaL_Reg[] l, int nup);

        /*
        LUALIB_API void (luaL_pushmodule) (lua_State *L, const char *modname,int sizehint);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_pushmodule(LuaState L, string modname,int sizehint);

        /*
        LUALIB_API void *(luaL_testudata) (lua_State *L, int ud, const char *tname);
        */
        [DllImport(LuaJitLibName)] public static extern VoidPtr luaL_testudata(LuaState L, int ud, string tname);

        /*
        LUALIB_API void (luaL_buffinit) (lua_State *L, luaL_Buffer *B);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_buffinit(LuaState L, ref LuaL_Buffer B);

        /*
        LUALIB_API char *(luaL_prepbuffer) (luaL_Buffer *B);
        */
        [DllImport(LuaJitLibName)] public static extern IntPtr luaL_prepbuffer(ref LuaL_Buffer B);

        /*
        LUALIB_API void (luaL_addlstring) (luaL_Buffer *B, const char *s, size_t l);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_addlstring(ref LuaL_Buffer B, string s, int l);

        /*
        LUALIB_API void (luaL_addstring) (luaL_Buffer *B, const char *s);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_addstring(ref LuaL_Buffer B, string s);

        /*
        LUALIB_API void (luaL_addvalue) (luaL_Buffer *B);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_addvalue(ref LuaL_Buffer B);

        /*
        LUALIB_API void (luaL_pushresult) (luaL_Buffer *B);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_pushresult(ref LuaL_Buffer B);
#endregion lauxlib.h

#region lualib.h


        /*
        LUALIB_API int luaopen_base(lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int luaopen_base(LuaState L);

        /*
        LUALIB_API int luaopen_math(lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int luaopen_math(LuaState L);

        /*
        LUALIB_API int luaopen_string(lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int luaopen_string(LuaState L);

        /*
        LUALIB_API int luaopen_table(lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int luaopen_table(LuaState L);

        /*
        LUALIB_API int luaopen_io(lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int luaopen_io(LuaState L);

        /*
        LUALIB_API int luaopen_os(lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int luaopen_os(LuaState L);

        /*
        LUALIB_API int luaopen_package(lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int luaopen_package(LuaState L);

        /*
        LUALIB_API int luaopen_debug(lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int luaopen_debug(LuaState L);

        /*
        LUALIB_API int luaopen_bit(lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int luaopen_bit(LuaState L);

        /*
        LUALIB_API int luaopen_jit(lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int luaopen_jit(LuaState L);

        /*
        LUALIB_API int luaopen_ffi(lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int luaopen_ffi(LuaState L);

        /*
        LUALIB_API int luaopen_string_buffer(lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int luaopen_string_buffer(LuaState L);

        /*
        LUALIB_API void luaL_openlibs(lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern void luaL_openlibs(LuaState L);
#endregion lualib.h

#region from luajit's lua.h
        public const int LUA_TNONE = -1;
        public const int LUA_MULTRET = -1;
#endregion

#region luaconf.h
        public const int LUA_IDSIZE = 60;
#endregion
            
#region lua.h

        public const int LUA_VERSION_NUM = 501;
        public const int LUA_OK = 0;
        public const int LUA_YIELD = 1;
        public const int LUA_ERRRUN = 2;
        public const int LUA_ERRSYNTAX = 3;
        public const int LUA_ERRMEM = 4;
        public const int LUA_ERRERR = 5;
        public const int LUA_TNIL = 0;
        public const int LUA_TBOOLEAN = 1;
        public const int LUA_TLIGHTUSERDATA = 2;
        public const int LUA_TNUMBER = 3;
        public const int LUA_TSTRING = 4;
        public const int LUA_TTABLE = 5;
        public const int LUA_TFUNCTION = 6;
        public const int LUA_TUSERDATA = 7;
        public const int LUA_TTHREAD = 8;
        public const int LUA_MINSTACK = 20;
        public const int LUA_GCSTOP = 0;
        public const int LUA_GCRESTART = 1;
        public const int LUA_GCCOLLECT = 2;
        public const int LUA_GCCOUNT = 3;
        public const int LUA_GCCOUNTB = 4;
        public const int LUA_GCSTEP = 5;
        public const int LUA_GCSETPAUSE = 6;
        public const int LUA_GCSETSTEPMUL = 7;
        public const int LUA_GCISRUNNING = 9;
        public const int LUA_HOOKCALL = 0;
        public const int LUA_HOOKRET = 1;
        public const int LUA_HOOKLINE = 2;
        public const int LUA_HOOKCOUNT = 3;
        public const int LUA_HOOKTAILRET = 4;

        public const int LUA_REGISTRYINDEX = -10000;
        public const int LUA_ENVIRONINDEX = -10001;
        public const int LUA_GLOBALSINDEX = -10002;

        /*
        LUA_API lua_State *(lua_newstate) (lua_Alloc f, void *ud);
        */
        [DllImport(LuaJitLibName)] public static extern LuaState lua_newstate(Alloc f, VoidPtr ud);

        /*
        LUA_API void       (lua_close) (lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern void       lua_close(LuaState L);

        /*
        LUA_API lua_State *(lua_newthread) (lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern LuaState lua_newthread(LuaState L);

        /*
        LUA_API lua_CFunction (lua_atpanic) (lua_State *L, lua_CFunction panicf);
        */
        [DllImport(LuaJitLibName)] public static extern LuaCFunction lua_atpanic(LuaState L, LuaCFunction panicf);

        /*
        LUA_API int   (lua_gettop) (lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int   lua_gettop(LuaState L);

        /*
        LUA_API void  (lua_settop) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_settop(LuaState L, int idx);

        /*
        LUA_API void  (lua_pushvalue) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_pushvalue(LuaState L, int idx);

        /*
        LUA_API void  (lua_remove) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_remove(LuaState L, int idx);

        /*
        LUA_API void  (lua_insert) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_insert(LuaState L, int idx);

        /*
        LUA_API void  (lua_replace) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_replace(LuaState L, int idx);

        /*
        LUA_API int   (lua_checkstack) (lua_State *L, int sz);
        */
        [DllImport(LuaJitLibName)] public static extern int   lua_checkstack(LuaState L, int sz);

        /*
        LUA_API void  (lua_xmove) (lua_State *from, lua_State *to, int n);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_xmove(LuaState from, LuaState to, int n);

        /*
        LUA_API int             (lua_isnumber) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern int             lua_isnumber(LuaState L, int idx);

        /*
        LUA_API int             (lua_isstring) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern int             lua_isstring(LuaState L, int idx);

        /*
        LUA_API int             (lua_iscfunction) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern int             lua_iscfunction(LuaState L, int idx);

        /*
        LUA_API int             (lua_isuserdata) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern int             lua_isuserdata(LuaState L, int idx);

        /*
        LUA_API int             (lua_type) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern int             lua_type(LuaState L, int idx);

        /*
        LUA_API const char     *(lua_typename) (lua_State *L, int tp);
        */
        [DllImport(LuaJitLibName)] public static extern CString lua_typename(LuaState L, int tp);

        /*
        LUA_API int            (lua_equal) (lua_State *L, int idx1, int idx2);
        */
        [DllImport(LuaJitLibName)] public static extern int            lua_equal(LuaState L, int idx1, int idx2);

        /*
        LUA_API int            (lua_rawequal) (lua_State *L, int idx1, int idx2);
        */
        [DllImport(LuaJitLibName)] public static extern int            lua_rawequal(LuaState L, int idx1, int idx2);

        /*
        LUA_API int            (lua_lessthan) (lua_State *L, int idx1, int idx2);
        */
        [DllImport(LuaJitLibName)] public static extern int            lua_lessthan(LuaState L, int idx1, int idx2);

        /*
        LUA_API lua_Number      (lua_tonumber) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern double      lua_tonumber(LuaState L, int idx);

        /*
        LUA_API lua_Integer     (lua_tointeger) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern int     lua_tointeger(LuaState L, int idx);

        /*
        LUA_API int             (lua_toboolean) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern int             lua_toboolean(LuaState L, int idx);

        /*
        LUA_API const char     *(lua_tolstring) (lua_State *L, int idx, size_t *len);
        */
        // [return: MarshalAs(UnmanagedType.LPStr)]
        [DllImport(LuaJitLibName, CallingConvention = CallingConvention.Cdecl)] public static extern CString lua_tolstring(LuaState L, int idx, IntPtr len);

        /*
        LUA_API size_t          (lua_objlen) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern int          lua_objlen(LuaState L, int idx);

        /*
        LUA_API lua_CFunction   (lua_tocfunction) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern LuaCFunction   lua_tocfunction(LuaState L, int idx);

        /*
        LUA_API void	       *(lua_touserdata) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern VoidPtr lua_touserdata(LuaState L, int idx);

        /*
        LUA_API lua_State      *(lua_tothread) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern LuaState lua_tothread(LuaState L, int idx);

        /*
        LUA_API const void     *(lua_topointer) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern VoidPtr lua_topointer(LuaState L, int idx);

        /*
        LUA_API void  (lua_pushnil) (lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_pushnil(LuaState L);

        /*
        LUA_API void  (lua_pushnumber) (lua_State *L, lua_Number n);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_pushnumber(LuaState L, double n);

        /*
        LUA_API void  (lua_pushinteger) (lua_State *L, lua_Integer n);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_pushinteger(LuaState L, int n);

        /*
        LUA_API void  (lua_pushlstring) (lua_State *L, const char *s, size_t l);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_pushlstring(LuaState L, string s, int l);

        /*
        LUA_API void  (lua_pushstring) (lua_State *L, const char *s);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_pushstring(LuaState L, string s);

        /*
        LUA_API void  (lua_pushstring) (lua_State *L, const char *s);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_pushstring(LuaState L, IntPtr s);

        /*
        LUA_API const char *(lua_pushvfstring) (lua_State *L, const char *fmt,va_list argp);
        */
        //[DllImport(LuaJitLibName)] public static extern string lua_pushvfstring(lua_State_ptr L, string fmt,va_list argp);

        /*
        LUA_API const char *(lua_pushfstring) (lua_State *L, const char *fmt, ...);
        */
        //[DllImport(LuaJitLibName)] public static extern string lua_pushfstring(lua_State_ptr L, string fmt, ...);

        /*
        LUA_API void  (lua_pushcclosure) (lua_State *L, lua_CFunction fn, int n);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_pushcclosure(LuaState L, LuaCFunction fn, int n);

        /*
        LUA_API void  (lua_pushcclosure) (lua_State *L, lua_CFunction fn, int n);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_pushcclosure(LuaState L, IntPtr fn, int n);

        /*
        LUA_API void  (lua_pushboolean) (lua_State *L, int b);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_pushboolean(LuaState L, int b);

        /*
        LUA_API void  (lua_pushlightuserdata) (lua_State *L, void *p);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_pushlightuserdata(LuaState L, VoidPtr p);

        /*
        LUA_API int   (lua_pushthread) (lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int   lua_pushthread(LuaState L);

        /*
        LUA_API void  (lua_gettable) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_gettable(LuaState L, int idx);

        /*
        LUA_API void  (lua_getfield) (lua_State *L, int idx, const char *k);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_getfield(LuaState L, int idx, string k);

        /*
        LUA_API void  (lua_getfield) (lua_State *L, int idx, const char *k);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_getfield(LuaState L, int idx, CString k);

        /*
        LUA_API void  (lua_rawget) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_rawget(LuaState L, int idx);

        /*
        LUA_API void  (lua_rawgeti) (lua_State *L, int idx, int n);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_rawgeti(LuaState L, int idx, int n);

        /*
        LUA_API void  (lua_createtable) (lua_State *L, int narr, int nrec);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_createtable(LuaState L, int narr, int nrec);

        /*
        LUA_API void *(lua_newuserdata) (lua_State *L, size_t sz);
        */
        [DllImport(LuaJitLibName)] public static extern VoidPtr lua_newuserdata(LuaState L, int sz);

        /*
        LUA_API int   (lua_getmetatable) (lua_State *L, int objindex);
        */
        [DllImport(LuaJitLibName)] public static extern int   lua_getmetatable(LuaState L, int objindex);

        /*
        LUA_API void  (lua_getfenv) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_getfenv(LuaState L, int idx);

        /*
        LUA_API void  (lua_settable) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_settable(LuaState L, int idx);

        /*
        LUA_API void  (lua_setfield) (lua_State *L, int idx, const char *k);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_setfield(LuaState L, int idx, string k);

        /*
        LUA_API void  (lua_rawset) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_rawset(LuaState L, int idx);

        /*
        LUA_API void  (lua_rawseti) (lua_State *L, int idx, int n);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_rawseti(LuaState L, int idx, int n);

        /*
        LUA_API int   (lua_setmetatable) (lua_State *L, int objindex);
        */
        [DllImport(LuaJitLibName)] public static extern int   lua_setmetatable(LuaState L, int objindex);

        /*
        LUA_API int   (lua_setfenv) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern int   lua_setfenv(LuaState L, int idx);

        /*
        LUA_API void  (lua_call) (lua_State *L, int nargs, int nresults);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_call(LuaState L, int nargs, int nresults);

        /*
        LUA_API int   (lua_pcall) (lua_State *L, int nargs, int nresults, int errfunc);
        */
        [DllImport(LuaJitLibName)] public static extern int   lua_pcall(LuaState L, int nargs, int nresults, int errfunc);

        /*
        LUA_API int   (lua_cpcall) (lua_State *L, lua_CFunction func, void *ud);
        */
        [DllImport(LuaJitLibName)] public static extern int   lua_cpcall(LuaState L, LuaCFunction func, VoidPtr ud);

        /*
        LUA_API int   (lua_load) (lua_State *L, lua_Reader reader, void *dt,const char *chunkname);
        */
        [DllImport(LuaJitLibName)] public static extern int   lua_load(LuaState L, Reader reader, VoidPtr dt,string chunkname);

        /*
        LUA_API int (lua_dump) (lua_State *L, lua_Writer writer, void *data);
        */
        [DllImport(LuaJitLibName)] public static extern int lua_dump(LuaState L, LuaWriter luaWriter, VoidPtr data);

        /*
        LUA_API int  (lua_yield) (lua_State *L, int nresults);
        */
        [DllImport(LuaJitLibName)] public static extern int  lua_yield(LuaState L, int nresults);

        /*
        LUA_API int  (lua_resume) (lua_State *L, int narg);
        */
        [DllImport(LuaJitLibName)] public static extern int  lua_resume(LuaState L, int narg);

        /*
        LUA_API int  (lua_status) (lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int  lua_status(LuaState L);

        /*
        LUA_API int (lua_gc) (lua_State *L, int what, int data);
        */
        [DllImport(LuaJitLibName)] public static extern int lua_gc(LuaState L, int what, int data);

        /*
        LUA_API int   (lua_error) (lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int   lua_error(LuaState L);

        /*
        LUA_API int   (lua_next) (lua_State *L, int idx);
        */
        [DllImport(LuaJitLibName)] public static extern int   lua_next(LuaState L, int idx);

        /*
        LUA_API void  (lua_concat) (lua_State *L, int n);
        */
        [DllImport(LuaJitLibName)] public static extern void  lua_concat(LuaState L, int n);

        /*
        LUA_API lua_Alloc (lua_getallocf) (lua_State *L, void **ud);
        */
        [DllImport(LuaJitLibName)] public static extern Alloc lua_getallocf(LuaState L, VoidPtrPtr ud);

        /*
        LUA_API void lua_setallocf (lua_State *L, lua_Alloc f, void *ud);
        */
        [DllImport(LuaJitLibName)] public static extern void lua_setallocf(LuaState L, Alloc f, VoidPtr ud);

        /*
        LUA_API void lua_setlevel	(lua_State *from, lua_State *to);
        */
        [DllImport(LuaJitLibName)] public static extern void lua_setlevel(LuaState from, LuaState to);

        /*
        LUA_API int lua_getstack (lua_State *L, int level, lua_Debug *ar);
        */
        [DllImport(LuaJitLibName)] public static extern unsafe int lua_getstack(LuaState L, int level, LuaDebug* ar);

        /*
        LUA_API int lua_getinfo (lua_State *L, const char *what, lua_Debug *ar);
        */
        [DllImport(LuaJitLibName)] public static extern unsafe int lua_getinfo(LuaState L, string what, LuaDebug* ar);

        /*
        LUA_API const char *lua_getlocal (lua_State *L, const lua_Debug *ar, int n);
        */
        [DllImport(LuaJitLibName)] public static extern unsafe string lua_getlocal(LuaState L, LuaDebug* ar, int n);

        /*
        LUA_API const char *lua_setlocal (lua_State *L, const lua_Debug *ar, int n);
        */
        [DllImport(LuaJitLibName)] public static extern unsafe string lua_setlocal(LuaState L, LuaDebug* ar, int n);

        /*
        LUA_API const char *lua_getupvalue (lua_State *L, int funcindex, int n);
        */
        [DllImport(LuaJitLibName)] public static extern string lua_getupvalue(LuaState L, int funcindex, int n);

        /*
        LUA_API const char *lua_setupvalue (lua_State *L, int funcindex, int n);
        */
        [DllImport(LuaJitLibName)] public static extern string lua_setupvalue(LuaState L, int funcindex, int n);

        /*
        LUA_API int lua_sethook (lua_State *L, lua_Hook func, int mask, int count);
        */
        [DllImport(LuaJitLibName)] public static extern int lua_sethook(LuaState L, LuaHook func, int mask, int count);

        /*
        LUA_API lua_Hook lua_gethook (lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern LuaHook lua_gethook(LuaState L);

        /*
        LUA_API int lua_gethookmask (lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int lua_gethookmask(LuaState L);

        /*
        LUA_API int lua_gethookcount (lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int lua_gethookcount(LuaState L);

        /*
        LUA_API void *lua_upvalueid (lua_State *L, int idx, int n);
        */
        [DllImport(LuaJitLibName)] public static extern VoidPtr lua_upvalueid(LuaState L, int idx, int n);

        /*
        LUA_API void lua_upvaluejoin (lua_State *L, int idx1, int n1, int idx2, int n2);
        */
        [DllImport(LuaJitLibName)] public static extern void lua_upvaluejoin(LuaState L, int idx1, int n1, int idx2, int n2);

        /*
        LUA_API int lua_loadx (lua_State *L, lua_Reader reader, void *dt,const char *chunkname, const char *mode);
        */
        [DllImport(LuaJitLibName)] public static extern int lua_loadx(LuaState L, Reader reader, VoidPtr dt,string chunkname, string mode);

        /*
        LUA_API void lua_copy (lua_State *L, int fromidx, int toidx);
        */
        [DllImport(LuaJitLibName)] public static extern void lua_copy(LuaState L, int fromidx, int toidx);

        /*
        LUA_API lua_Number lua_tonumberx (lua_State *L, int idx, int *isnum);
        */
        [DllImport(LuaJitLibName)] public static extern double lua_tonumberx(LuaState L, int idx, ref int isnum);

        /*
        LUA_API lua_Integer lua_tointegerx (lua_State *L, int idx, int *isnum);
        */
        [DllImport(LuaJitLibName)] public static extern int lua_tointegerx(LuaState L, int idx, ref int isnum);

        /*
        LUA_API int lua_isyieldable (lua_State *L);
        */
        [DllImport(LuaJitLibName)] public static extern int lua_isyieldable(LuaState L);
#endregion lua.h



    }
}
