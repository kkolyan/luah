namespace Luah
{
    public unsafe delegate void LuaHook(LuaState state, LuaDebug* debug);
}