using System;

namespace Luah
{
    public static class LuaMacrosAsExtensions
    {
#region lua.h macros

        /**
         * #define lua_pop(L,n)		lua_settop(L, -(n)-1)
         */
        public static void lua_pop(this LuaState thiz, int n)
        {
            thiz.lua_settop(-n - 1);
        }

        /**
         * #define lua_newtable(L)		lua_createtable(L, 0, 0)
         */
        public static void lua_newtable(this LuaState thiz)
        {
            thiz.lua_createtable(0, 0);
        }

        /**
         * #define lua_register(L,n,f) (lua_pushcfunction(L, (f)), lua_setglobal(L, (n)))
         */
        public static void lua_register(this LuaState thiz, string n, LuaCFunction f)
        {
            thiz.lua_pushcfunction(f);
            thiz.lua_setglobal(n);
        }

        /**
         * #define lua_pushcfunction(L,f)	lua_pushcclosure(L, (f), 0)
         */
        public static void lua_pushcfunction(this LuaState thiz, LuaCFunction f)
        {
            thiz.lua_pushcclosure(f, 0);
        }

        /**
         * #define lua_pushcfunction(L,f)	lua_pushcclosure(L, (f), 0)
         */
        public static void lua_pushcfunction(this LuaState thiz, IntPtr f)
        {
            thiz.lua_pushcclosure(f, 0);
        }

        /**
         * #define lua_strlen(L,i)		lua_objlen(L, (i))
         */
        public static int lua_strlen(this LuaState thiz, int i)
        {
            return thiz.lua_objlen(i);
        }

        /** #define lua_isfunction(L,n)            (lua_type(L, (n)) == LUA_TFUNCTION) */
        public static bool lua_isfunction(this LuaState thiz, int n)
        {
            return thiz.lua_type(n) == Lua.LUA_TFUNCTION;
        }

        /** #define lua_istable(L,n)               (lua_type(L, (n)) == LUA_TTABLE) */
        public static bool lua_istable(this LuaState thiz, int n)
        {
            return thiz.lua_type(n) == Lua.LUA_TTABLE;
        }

        /** #define lua_islightuserdata(L,n)       (lua_type(L, (n)) == LUA_TLIGHTUSERDATA) */
        public static bool lua_islightuserdata(this LuaState thiz, int n)
        {
            return thiz.lua_type(n) == Lua.LUA_TLIGHTUSERDATA;
        }

        /** #define lua_isnil(L,n)                 (lua_type(L, (n)) == LUA_TNIL) */
        public static bool lua_isnil(this LuaState thiz, int n)
        {
            return thiz.lua_type(n) == Lua.LUA_TNIL;
        }

        /** #define lua_isboolean(L,n)             (lua_type(L, (n)) == LUA_TBOOLEAN) */
        public static bool lua_isboolean(this LuaState thiz, int n)
        {
            return thiz.lua_type(n) == Lua.LUA_TBOOLEAN;
        }

        /** #define lua_isthread(L,n)              (lua_type(L, (n)) == LUA_TTHREAD) */
        public static bool lua_isthread(this LuaState thiz, int n)
        {
            return thiz.lua_type(n) == Lua.LUA_TTHREAD;
        }

        /** #define lua_isnone(L,n)                (lua_type(L, (n)) == LUA_TNONE) */
        public static bool lua_isnone(this LuaState thiz, int n)
        {
            return thiz.lua_type(n) == Lua.LUA_TNONE;
        }

        /* #define lua_isnoneornil(L, n)          (lua_type(L, (n)) <= 0) */
        public static bool lua_isnoneornil(this LuaState thiz, int n)
        {
            return thiz.lua_type(n) <= 0;
        }

        /** #define lua_pushliteral(L, s)          lua_pushlstring(L, "" s, (sizeof(s)/sizeof(char))-1) */
        public static void lua_pushliteral(this LuaState thiz, string s)
        {
            thiz.lua_pushlstring(s, s.Length);
        }

        /** #define lua_setglobal(L,s)             lua_setfield(L, LUA_GLOBALSINDEX, (s)) */
        public static void lua_setglobal(this LuaState thiz, string s)
        {
            thiz.lua_setfield(Lua.LUA_GLOBALSINDEX, s);
        }

        /** #define lua_getglobal(L,s)             lua_getfield(L, LUA_GLOBALSINDEX, (s)) */
        public static void lua_getglobal(this LuaState thiz, string s)
        {
            thiz.lua_getfield(Lua.LUA_GLOBALSINDEX, s);
        }

        /** #define lua_getglobal(L,s)             lua_getfield(L, LUA_GLOBALSINDEX, (s)) */
        public static void lua_getglobal(this LuaState thiz, CString s)
        {
            thiz.lua_getfield(Lua.LUA_GLOBALSINDEX, s);
        }

        /** #define lua_tostring(L,i)              lua_tolstring(L, (i), NULL) */
        public static CString lua_tostring(this LuaState thiz, int i)
        {
            return thiz.lua_tolstring(i, IntPtr.Zero);
        }

#endregion

#region lauxlib.h macros

        /** #define luaL_argcheck(L, cond,numarg,extramsg) ((void)((cond) || luaL_argerror(L, (numarg), (extramsg))))            */
        public static void luaL_argcheck(this LuaState thiz, bool cond, int numarg, string extramsg)
        {
            if (!cond)
            {
                thiz.luaL_argerror(numarg, extramsg);
            }
        }

        /** #define luaL_checkstring(L,n)	               (luaL_checklstring(L, (n), NULL))                                     */
        public static CString luaL_checkstring(this LuaState thiz, int n)
        {
            return thiz.luaL_checklstring(n, IntPtr.Zero);
        }

        /** #define luaL_optstring(L,n,d)	               (luaL_optlstring(L, (n), (d), NULL))                                  */
        public static CString luaL_optstring(this LuaState thiz, int n, string d)
        {
            return thiz.luaL_optlstring(n, d, IntPtr.Zero);
        }

        /** #define luaL_checkint(L,n)	                   ((int)luaL_checkinteger(L, (n)))                                      */
        public static int luaL_checkint(this LuaState thiz, int n)
        {
            return thiz.luaL_checkinteger(n);
        }

        /** #define luaL_optint(L,n,d)	                   ((int)luaL_optinteger(L, (n), (d)))                                   */
        public static int luaL_optint(this LuaState thiz, int n, int d)
        {
            return thiz.luaL_optinteger(n, d);
        }

        // methods for "long" type looks unsafe for C#, because `lua.h` use ptrdiff_t for integers, which doesn't always fit for Int64

        /* #define luaL_checklong(L,n)	                   ((long)luaL_checkinteger(L, (n)))                                     */
        // public static long luaL_checklong(int n)	                   {return ((long)luaL_checkinteger( (this LuaState thiz, n)))                         ;}

        /* #define luaL_optlong(L,n,d)	                   ((long)luaL_optinteger(L, (n), (d)))                                  */
        //public static long luaL_optlong(int n,long d)	                   {return ((long)luaL_optinteger( (n), (this LuaState thiz, d)))                      ;}


        /** #define luaL_typename(L,i)	                   lua_typename(L, lua_type(L,(i)))                                      */
        public static CString luaL_typename(this LuaState thiz, int i)
        {
            return thiz.lua_typename(thiz.lua_type(i));
        }

        /** #define luaL_dofile(L, fn)                     (luaL_loadfile(L, fn) || lua_pcall(L, 0, LUA_MULTRET, 0))             */
        public static int luaL_dofile(this LuaState thiz, string fn)
        {
            int result = thiz.luaL_loadfile(fn);
            if (result == Lua.LUA_OK)
            {
                return result;
            }

            return thiz.lua_pcall(0, Lua.LUA_MULTRET, 0);
        }

        /** #define luaL_dostring(L, s)                    (luaL_loadstring(L, s) || lua_pcall(L, 0, LUA_MULTRET, 0))            */
        public static int luaL_dostring(this LuaState thiz, string s)
        {
            int result = thiz.luaL_loadstring(s);
            if (result == Lua.LUA_OK)
            {
                return result;
            }

            return thiz.lua_pcall(0, Lua.LUA_MULTRET, 0);
        }

        /** #define luaL_getmetatable(L,n)	               (lua_getfield(L, LUA_REGISTRYINDEX, (n)))                             */
        public static void luaL_getmetatable(this LuaState thiz, string n)
        {
            thiz.lua_getfield(Lua.LUA_REGISTRYINDEX, n);
        }

        /* #define luaL_opt(L,f,n,d)	                   (lua_isnoneornil(L,(n)) ? (d) : f(L,(n)))                             */
        // doesn't make sense for C#

#endregion
    }
}