using System;
using System.Diagnostics.CodeAnalysis;

namespace Luah
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public static class LuaFunctionsAsExtensions
    {
#region generated from lua.h

        public static unsafe string lua_getlocal(this LuaState thiz, LuaDebug* ar, int n)
        {
            return Lua.lua_getlocal(thiz, ar, n);
        }

        public static unsafe string lua_setlocal(this LuaState thiz, LuaDebug* ar, int n)
        {
            return Lua.lua_setlocal(thiz, ar, n);
        }

        public static string lua_getupvalue(this LuaState thiz, int funcindex, int n)
        {
            return Lua.lua_getupvalue(thiz, funcindex, n);
        }

        public static string lua_setupvalue(this LuaState thiz, int funcindex, int n)
        {
            return Lua.lua_setupvalue(thiz, funcindex, n);
        }

        public static int lua_sethook(this LuaState thiz, LuaHook func, int mask, int count)
        {
            return Lua.lua_sethook(thiz, func, mask, count);
        }

        public static LuaHook lua_gethook(this LuaState thiz)
        {
            return Lua.lua_gethook(thiz);
        }

        public static int lua_gethookmask(this LuaState thiz)
        {
            return Lua.lua_gethookmask(thiz);
        }

        public static int lua_gethookcount(this LuaState thiz)
        {
            return Lua.lua_gethookcount(thiz);
        }

        public static VoidPtr lua_upvalueid(this LuaState thiz, int idx, int n)
        {
            return Lua.lua_upvalueid(thiz, idx, n);
        }

        public static void lua_upvaluejoin(this LuaState thiz, int idx1, int n1, int idx2, int n2)
        {
            Lua.lua_upvaluejoin(thiz, idx1, n1, idx2, n2);
        }

        public static int lua_loadx(this LuaState thiz, Reader reader, VoidPtr dt, string chunkname, string mode)
        {
            return Lua.lua_loadx(thiz, reader, dt, chunkname, mode);
        }

        public static void lua_copy(this LuaState thiz, int fromidx, int toidx)
        {
            Lua.lua_copy(thiz, fromidx, toidx);
        }

        public static double lua_tonumberx(this LuaState thiz, int idx, ref int isnum)
        {
            return Lua.lua_tonumberx(thiz, idx, ref isnum);
        }

        public static int lua_tointegerx(this LuaState thiz, int idx, ref int isnum)
        {
            return Lua.lua_tointegerx(thiz, idx, ref isnum);
        }

        public static bool lua_isyieldable(this LuaState thiz)
        {
            return Lua.lua_isyieldable(thiz) != 0;
        }

        public static bool lua_getmetatable(this LuaState thiz, int objindex)
        {
            return Lua.lua_getmetatable(thiz, objindex) != 0;
        }

        public static void lua_getfenv(this LuaState thiz, int idx)
        {
            Lua.lua_getfenv(thiz, idx);
        }

        public static void lua_settable(this LuaState thiz, int idx)
        {
            Lua.lua_settable(thiz, idx);
        }

        public static void lua_setfield(this LuaState thiz, int idx, string k)
        {
            Lua.lua_setfield(thiz, idx, k);
        }

        public static void lua_rawset(this LuaState thiz, int idx)
        {
            Lua.lua_rawset(thiz, idx);
        }

        public static void lua_rawseti(this LuaState thiz, int idx, int n)
        {
            Lua.lua_rawseti(thiz, idx, n);
        }

        public static int lua_setmetatable(this LuaState thiz, int objindex)
        {
            return Lua.lua_setmetatable(thiz, objindex);
        }

        public static int lua_setfenv(this LuaState thiz, int idx)
        {
            return Lua.lua_setfenv(thiz, idx);
        }

        public static void lua_call(this LuaState thiz, int nargs, int nresults)
        {
            Lua.lua_call(thiz, nargs, nresults);
        }

        public static int lua_pcall(this LuaState thiz, int nargs, int nresults, int errfunc)
        {
            return Lua.lua_pcall(thiz, nargs, nresults, errfunc);
        }

        public static int lua_cpcall(this LuaState thiz, LuaCFunction func, VoidPtr ud)
        {
            return Lua.lua_cpcall(thiz, func, ud);
        }

        public static int lua_pcall(this LuaState thiz, int nargs, int nresults)
        {
            return Lua.lua_pcall(thiz, nargs, nresults, 0);
        }

        public static int lua_load(this LuaState thiz, Reader reader, VoidPtr dt, string chunkname)
        {
            return Lua.lua_load(thiz, reader, dt, chunkname);
        }

        public static int lua_dump(this LuaState thiz, LuaWriter luaWriter, VoidPtr data)
        {
            return Lua.lua_dump(thiz, luaWriter, data);
        }

        public static int lua_yield(this LuaState thiz, int nresults)
        {
            return Lua.lua_yield(thiz, nresults);
        }

        public static int lua_resume(this LuaState thiz, int narg)
        {
            return Lua.lua_resume(thiz, narg);
        }

        public static int lua_status(this LuaState thiz)
        {
            return Lua.lua_status(thiz);
        }

        public static int lua_gc(this LuaState thiz, int what, int data)
        {
            return Lua.lua_gc(thiz, what, data);
        }

        public static int lua_error(this LuaState thiz)
        {
            return Lua.lua_error(thiz);
        }

        public static bool lua_next(this LuaState thiz, int idx)
        {
            return Lua.lua_next(thiz, idx) != 0;
        }

        public static void lua_concat(this LuaState thiz, int n)
        {
            Lua.lua_concat(thiz, n);
        }

        public static Alloc lua_getallocf(this LuaState thiz, VoidPtrPtr ud)
        {
            return Lua.lua_getallocf(thiz, ud);
        }

        public static void lua_setallocf(this LuaState thiz, Alloc f, VoidPtr ud)
        {
            Lua.lua_setallocf(thiz, f, ud);
        }

        public static void lua_setlevel(this LuaState thiz, LuaState from, LuaState to)
        {
            Lua.lua_setlevel(from, to);
        }

        public static unsafe int lua_getstack(this LuaState thiz, int level, LuaDebug* ar)
        {
            return Lua.lua_getstack(thiz, level, ar);
        }

        public static unsafe int lua_getinfo(this LuaState thiz, string what, LuaDebug* ar)
        {
            return Lua.lua_getinfo(thiz, what, ar);
        }

        public static int lua_lessthan(this LuaState thiz, int idx1, int idx2)
        {
            return Lua.lua_lessthan(thiz, idx1, idx2);
        }

        public static double lua_tonumber(this LuaState thiz, int idx)
        {
            return Lua.lua_tonumber(thiz, idx);
        }

        public static int lua_tointeger(this LuaState thiz, int idx)
        {
            return Lua.lua_tointeger(thiz, idx);
        }

        public static bool lua_toboolean(this LuaState thiz, int idx)
        {
            return Lua.lua_toboolean(thiz, idx) != 0;
        }

        public static CString lua_tolstring(this LuaState thiz, int idx, IntPtr len)
        {
            return Lua.lua_tolstring(thiz, idx, len);
        }

        public static int lua_objlen(this LuaState thiz, int idx)
        {
            return Lua.lua_objlen(thiz, idx);
        }

        public static LuaCFunction lua_tocfunction(this LuaState thiz, int idx)
        {
            return Lua.lua_tocfunction(thiz, idx);
        }

        public static VoidPtr lua_touserdata(this LuaState thiz, int idx)
        {
            return Lua.lua_touserdata(thiz, idx);
        }

        public static LuaState lua_tothread(this LuaState thiz, int idx)
        {
            return Lua.lua_tothread(thiz, idx);
        }

        public static VoidPtr lua_topointer(this LuaState thiz, int idx)
        {
            return Lua.lua_topointer(thiz, idx);
        }

        public static void lua_pushnil(this LuaState thiz)
        {
            Lua.lua_pushnil(thiz);
        }

        public static void lua_pushnumber(this LuaState thiz, double n)
        {
            Lua.lua_pushnumber(thiz, n);
        }

        public static void lua_pushnumber(this LuaState thiz, int n)
        {
            Lua.lua_pushnumber(thiz, n);
        }

        public static void lua_pushinteger(this LuaState thiz, int n)
        {
            Lua.lua_pushinteger(thiz, n);
        }

        public static void lua_pushlstring(this LuaState thiz, string s, int l)
        {
            Lua.lua_pushlstring(thiz, s, l);
        }

        public static void lua_pushstring(this LuaState thiz, string s)
        {
            Lua.lua_pushstring(thiz, s);
        }

        public static void lua_pushstring(this LuaState thiz, IntPtr s)
        {
            Lua.lua_pushstring(thiz, s);
        }

        public static void lua_pushcclosure(this LuaState thiz, LuaCFunction fn, int n)
        {
            Lua.lua_pushcclosure(thiz, fn, n);
        }

        public static void lua_pushcclosure(this LuaState thiz, IntPtr fn, int n)
        {
            Lua.lua_pushcclosure(thiz, fn, n);
        }

        public static void lua_pushboolean(this LuaState thiz, bool b)
        {
            Lua.lua_pushboolean(thiz, b ? 1 : 0);
        }

        public static void lua_pushlightuserdata(this LuaState thiz, VoidPtr p)
        {
            Lua.lua_pushlightuserdata(thiz, p);
        }

        public static int lua_pushthread(this LuaState thiz)
        {
            return Lua.lua_pushthread(thiz);
        }

        public static void lua_gettable(this LuaState thiz, int idx)
        {
            Lua.lua_gettable(thiz, idx);
        }

        public static void lua_getfield(this LuaState thiz, int idx, string k)
        {
            Lua.lua_getfield(thiz, idx, k);
        }

        public static void lua_getfield(this LuaState thiz, int idx, CString k)
        {
            Lua.lua_getfield(thiz, idx, k);
        }

        public static void lua_rawget(this LuaState thiz, int idx)
        {
            Lua.lua_rawget(thiz, idx);
        }

        public static void lua_rawgeti(this LuaState thiz, int idx, int n)
        {
            Lua.lua_rawgeti(thiz, idx, n);
        }

        public static void lua_createtable(this LuaState thiz, int narr, int nrec)
        {
            Lua.lua_createtable(thiz, narr, nrec);
        }

        public static VoidPtr lua_newuserdata(this LuaState thiz, int sz)
        {
            return Lua.lua_newuserdata(thiz, sz);
        }

        public static int luaopen_bit(this LuaState thiz)
        {
            return Lua.luaopen_bit(thiz);
        }

        public static int luaopen_jit(this LuaState thiz)
        {
            return Lua.luaopen_jit(thiz);
        }

        public static int luaopen_ffi(this LuaState thiz)
        {
            return Lua.luaopen_ffi(thiz);
        }

        public static int luaopen_string_buffer(this LuaState thiz)
        {
            return Lua.luaopen_string_buffer(thiz);
        }

        public static void luaL_openlibs(this LuaState thiz)
        {
            Lua.luaL_openlibs(thiz);
        }

        public static LuaState lua_newstate(this LuaState thiz, Alloc f, VoidPtr ud)
        {
            return Lua.lua_newstate(f, ud);
        }

        public static void lua_close(this LuaState thiz)
        {
            Lua.lua_close(thiz);
        }

        public static LuaState lua_newthread(this LuaState thiz)
        {
            return Lua.lua_newthread(thiz);
        }

        public static LuaCFunction lua_atpanic(this LuaState thiz, LuaCFunction panicf)
        {
            return Lua.lua_atpanic(thiz, panicf);
        }

        public static int lua_gettop(this LuaState thiz)
        {
            return Lua.lua_gettop(thiz);
        }

        public static void lua_settop(this LuaState thiz, int idx)
        {
            Lua.lua_settop(thiz, idx);
        }

        public static void lua_pushvalue(this LuaState thiz, int idx)
        {
            Lua.lua_pushvalue(thiz, idx);
        }

        public static void lua_remove(this LuaState thiz, int idx)
        {
            Lua.lua_remove(thiz, idx);
        }

        public static void lua_insert(this LuaState thiz, int idx)
        {
            Lua.lua_insert(thiz, idx);
        }

        public static void lua_replace(this LuaState thiz, int idx)
        {
            Lua.lua_replace(thiz, idx);
        }

        public static int lua_checkstack(this LuaState thiz, int sz)
        {
            return Lua.lua_checkstack(thiz, sz);
        }

        public static void lua_xmove(this LuaState thiz, LuaState from, LuaState to, int n)
        {
            Lua.lua_xmove(from, to, n);
        }

        public static bool lua_isnumber(this LuaState thiz, int idx)
        {
            return Lua.lua_isnumber(thiz, idx) != 0;
        }

        public static bool lua_isstring(this LuaState thiz, int idx)
        {
            return Lua.lua_isstring(thiz, idx) != 0;
        }

        public static bool lua_iscfunction(this LuaState thiz, int idx)
        {
            return Lua.lua_iscfunction(thiz, idx) != 0;
        }

        public static bool lua_isuserdata(this LuaState thiz, int idx)
        {
            return Lua.lua_isuserdata(thiz, idx) != 0;
        }

        public static int lua_type(this LuaState thiz, int idx)
        {
            return Lua.lua_type(thiz, idx);
        }

        public static CString lua_typename(this LuaState thiz, int tp)
        {
            return Lua.lua_typename(thiz, tp);
        }

        public static int lua_equal(this LuaState thiz, int idx1, int idx2)
        {
            return Lua.lua_equal(thiz, idx1, idx2);
        }

        public static int lua_rawequal(this LuaState thiz, int idx1, int idx2)
        {
            return Lua.lua_rawequal(thiz, idx1, idx2);
        }

        public static string luaL_gsub(this LuaState thiz, string s, string p, string r)
        {
            return Lua.luaL_gsub(thiz, s, p, r);
        }

        public static string luaL_findtable(this LuaState thiz, int idx, string fname, int szhint)
        {
            return Lua.luaL_findtable(thiz, idx, fname, szhint);
        }

        public static int luaL_fileresult(this LuaState thiz, int stat, string fname)
        {
            return Lua.luaL_fileresult(thiz, stat, fname);
        }

        public static int luaL_execresult(this LuaState thiz, int stat)
        {
            return Lua.luaL_execresult(thiz, stat);
        }

        public static int luaL_loadfilex(this LuaState thiz, string filename, string mode)
        {
            return Lua.luaL_loadfilex(thiz, filename, mode);
        }

        public static int luaL_loadbufferx(this LuaState thiz, string buff, int sz, string name, string mode)
        {
            return Lua.luaL_loadbufferx(thiz, buff, sz, name, mode);
        }

        public static void luaL_traceback(this LuaState thiz, LuaState L1, string msg, int level)
        {
            Lua.luaL_traceback(thiz, L1, msg, level);
        }

        public static void luaL_traceback(this LuaState thiz, LuaState L1, CString msg, int level)
        {
            Lua.luaL_traceback(thiz, L1, msg, level);
        }

        public static void luaL_setfuncs(this LuaState thiz, LuaL_Reg[] l, int nup)
        {
            Lua.luaL_setfuncs(thiz, l, nup);
        }

        public static void luaL_pushmodule(this LuaState thiz, string modname, int sizehint)
        {
            Lua.luaL_pushmodule(thiz, modname, sizehint);
        }

        public static VoidPtr luaL_testudata(this LuaState thiz, int ud, string tname)
        {
            return Lua.luaL_testudata(thiz, ud, tname);
        }

        public static void luaL_setmetatable(this LuaState thiz, string tname)
        {
            Lua.luaL_newmetatable(thiz, tname);
            Lua.lua_setmetatable(thiz, -2);
        }

        public static void luaL_buffinit(this LuaState thiz, ref LuaL_Buffer B)
        {
            Lua.luaL_buffinit(thiz, ref B);
        }

        public static IntPtr luaL_prepbuffer(this LuaState thiz, ref LuaL_Buffer B)
        {
            return Lua.luaL_prepbuffer(ref B);
        }

        public static void luaL_addlstring(this LuaState thiz, ref LuaL_Buffer B, string s, int l)
        {
            Lua.luaL_addlstring(ref B, s, l);
        }

        public static void luaL_addstring(this LuaState thiz, ref LuaL_Buffer B, string s)
        {
            Lua.luaL_addstring(ref B, s);
        }

        public static void luaL_addvalue(this LuaState thiz, ref LuaL_Buffer B)
        {
            Lua.luaL_addvalue(ref B);
        }

        public static void luaL_pushresult(this LuaState thiz, ref LuaL_Buffer B)
        {
            Lua.luaL_pushresult(ref B);
        }

        public static int luaopen_base(this LuaState thiz)
        {
            return Lua.luaopen_base(thiz);
        }

        public static int luaopen_math(this LuaState thiz)
        {
            return Lua.luaopen_math(thiz);
        }

        public static int luaopen_string(this LuaState thiz)
        {
            return Lua.luaopen_string(thiz);
        }

        public static int luaopen_table(this LuaState thiz)
        {
            return Lua.luaopen_table(thiz);
        }

        public static int luaopen_io(this LuaState thiz)
        {
            return Lua.luaopen_io(thiz);
        }

        public static int luaopen_os(this LuaState thiz)
        {
            return Lua.luaopen_os(thiz);
        }

        public static int luaopen_package(this LuaState thiz)
        {
            return Lua.luaopen_package(thiz);
        }

        public static int luaopen_debug(this LuaState thiz)
        {
            return Lua.luaopen_debug(thiz);
        }

        public static void luaL_openlib(this LuaState thiz, string libname, LuaL_Reg[] l, int nup)
        {
            Lua.luaL_openlib(thiz, libname, l, nup);
        }

        public static void luaL_register(this LuaState thiz, string libname, LuaL_Reg[] l)
        {
            Lua.luaL_register(thiz, libname, l);
        }

        public static int luaL_getmetafield(this LuaState thiz, int obj, string e)
        {
            return Lua.luaL_getmetafield(thiz, obj, e);
        }

        public static bool luaL_callmeta(this LuaState thiz, int obj, string e)
        {
            return Lua.luaL_callmeta(thiz, obj, e) != 0;
        }

        public static int luaL_typerror(this LuaState thiz, int narg, string tname)
        {
            return Lua.luaL_typerror(thiz, narg, tname);
        }

        public static int luaL_argerror(this LuaState thiz, int numarg, string extramsg)
        {
            return Lua.luaL_argerror(thiz, numarg, extramsg);
        }

        public static CString luaL_checklstring(this LuaState thiz, int numArg, IntPtr l)
        {
            return Lua.luaL_checklstring(thiz, numArg, l);
        }

        public static CString luaL_optlstring(this LuaState thiz, int numArg, string def, IntPtr l)
        {
            return Lua.luaL_optlstring(thiz, numArg, def, l);
        }

        public static double luaL_checknumber(this LuaState thiz, int numArg)
        {
            return Lua.luaL_checknumber(thiz, numArg);
        }

        public static double luaL_optnumber(this LuaState thiz, int nArg, double def)
        {
            return Lua.luaL_optnumber(thiz, nArg, def);
        }

        public static int luaL_checkinteger(this LuaState thiz, int numArg)
        {
            return Lua.luaL_checkinteger(thiz, numArg);
        }

        public static int luaL_optinteger(this LuaState thiz, int nArg, int def)
        {
            return Lua.luaL_optinteger(thiz, nArg, def);
        }

        public static void luaL_checkstack(this LuaState thiz, int sz, string msg)
        {
            Lua.luaL_checkstack(thiz, sz, msg);
        }

        public static void luaL_checktype(this LuaState thiz, int narg, int t)
        {
            Lua.luaL_checktype(thiz, narg, t);
        }

        public static void luaL_checkany(this LuaState thiz, int narg)
        {
            Lua.luaL_checkany(thiz, narg);
        }

        public static int luaL_newmetatable(this LuaState thiz, string tname)
        {
            return Lua.luaL_newmetatable(thiz, tname);
        }

        public static VoidPtr luaL_checkudata(this LuaState thiz, int ud, string tname)
        {
            return Lua.luaL_checkudata(thiz, ud, tname);
        }

        public static void luaL_where(this LuaState thiz, int lvl)
        {
            Lua.luaL_where(thiz, lvl);
        }

        public static int luaL_error(this LuaState thiz, string fmt)
        {
            return Lua.luaL_error(thiz, fmt);
        }

        public static int luaL_checkoption(this LuaState thiz, int narg, string def, string[] lst)
        {
            return Lua.luaL_checkoption(thiz, narg, def, lst);
        }

        public static int luaL_ref(this LuaState thiz, int t)
        {
            return Lua.luaL_ref(thiz, t);
        }

        public static void luaL_unref(this LuaState thiz, int t, int @ref)
        {
            Lua.luaL_unref(thiz, t, @ref);
        }

        public static int luaL_loadfile(this LuaState thiz, string filename)
        {
            return Lua.luaL_loadfile(thiz, filename);
        }

        public static int luaL_loadbuffer(this LuaState thiz, string buff, int sz, string name)
        {
            return Lua.luaL_loadbuffer(thiz, buff, sz, name);
        }

        public static int luaL_loadstring(this LuaState thiz, string s)
        {
            return Lua.luaL_loadstring(thiz, s);
        }

        public static LuaState luaL_newstate(this LuaState thiz)
        {
            return Lua.luaL_newstate();
        }
        
#endregion
        
    }
}