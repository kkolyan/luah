namespace Luah.Reflection
{
    public interface ILuaUserdata
    {
        ILuaTable Metatable { get; }
        object Resolved { get; }
    }
}