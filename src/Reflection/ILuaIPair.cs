namespace Luah.Reflection
{
    public interface ILuaIPair : ILuaPair
    {
        int Index {get;}
    }
}