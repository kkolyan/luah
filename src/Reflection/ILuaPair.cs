using Luah.Debug;

namespace Luah.Reflection
{
    public interface ILuaPair
    {
        bool Named { get; }
        bool Indexed { get; }
        
        ILuaValue Value { get; }

        ILuaIPair AsIndexed();
        ILuaKPair AsNamed();
    }
}