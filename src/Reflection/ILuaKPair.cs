namespace Luah.Reflection
{
    public interface ILuaKPair : ILuaPair
    {
        string Key {get;}
    }
}