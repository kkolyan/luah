using System;

namespace Luah.Reflection
{
    public interface ILuaValue
    {
        string AsString();
        double AsNumber();
        bool AsBool();
        ILuaUserdata AsUserdata();
        ILuaTable AsTable();
        ILuaFunction AsFunction();
        
        bool IsNil();
        bool IsString();
        bool IsNumber();
        bool IsBool();
        bool IsUserdata();
        bool IsTable();
        bool IsFunction();
        bool IsLightUserdata();
        bool IsThread();
    }
}