using System.Collections.Generic;

namespace Luah.Reflection
{
    public interface ILuaTable
    {
        ILuaTable Metatable { get; }
        IEnumerable<ILuaPair> Items { get; }
        IEnumerable<ILuaKPair> NamedPairs { get; }
        IEnumerable<ILuaIPair> IndexedPairs { get; }
        
        // note that that's extremely inefficient.
        ILuaValue this[int index] { get; }
        
        // note that that's extremely inefficient.
        ILuaValue this[string key] { get; }
    }
}