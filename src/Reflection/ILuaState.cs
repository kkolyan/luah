using System.Collections.Generic;

namespace Luah.Reflection
{
    public interface ILuaState
    {
        List<ILuaValue> Stack { get; }
        ILuaTable Globals { get; }
    }
}