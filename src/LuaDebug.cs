using System;
using System.Runtime.InteropServices;

namespace Luah
{
    public unsafe struct LuaDebug {
        public int @event;
        public CString name;	/* (n) */
        public CString namewhat;	/* (n) `global', `local', `field', `method' */
        public CString what;	/* (S) `Lua', `C', `main', `tail' */
        public CString source;	/* (S) */
        public int currentline;	/* (l) */
        public int nups;		/* (u) number of upvalues */
        public int linedefined;	/* (S) */
        public int lastlinedefined;	/* (S) */
        public fixed byte short_src[Lua.LUA_IDSIZE]; /* (S) */
        /* private part */
        public int i_ci;  /* active function */

        public string short_src_AsString
        {
            get
            {
                fixed (byte * a = short_src)
                {
                    return Marshal.PtrToStringAnsi(new IntPtr(a));
                }
            }
        }
    };
}