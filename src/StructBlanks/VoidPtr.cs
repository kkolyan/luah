using System;

namespace Luah
{
    public struct VoidPtr
    {
        public IntPtr Value;

        public override string ToString()
        {
            return $"0x{Value.ToInt64():x}";
        }
    }
}