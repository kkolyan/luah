namespace Luah
{
    public struct LuaL_Reg {
        public string name;
        public LuaCFunction func;
    }
}